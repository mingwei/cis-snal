#!/bin/bash

#BRIX=brix.d.cs.uoregon.edu
#IX=ix.d.cs.uoregon.edu
#MON=noodles.duckdns.org
#URLS=(null $IX noodles.duckdns.org duckpi03.d.cs.uoregon.edu $BRIX $BRIX $BRIX $BRIX $BRIX)
#PORTS=(null 22210 22 22 2224 2225 2226 2227 2228)

for i in {1..8}
do
    #nc -zv ${URLS[$i]} ${PORTS[$i]};
    ssh -o ConnectTimeout=5 mingwei@duckpi0$i exit
    if [ $? -eq 0 ]
    then
        curl $MON:4567/update/duckpi0${i}/0
    else
        curl $MON:4567/update/duckpi0${i}/1
    fi
done
