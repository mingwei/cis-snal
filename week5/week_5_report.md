# Week 5 report

This week we continue practicing scripting. Note that you need to keep your Pi up and accessible as much as possible during the week. I will leave spare SD cards at the front office (I'm out of town Monday and Tuesday) in case you need to reinstall the OS on the original SD card. Note that the spares do not have user accounts or various other settings you have added since the beginning of class, so it may be a good idea to save a backup of your current SD card before you start.

## 1

System preparation: check the current process limit settings. Change the process limits of regular users on your Pi to 100. This should take a few minutes. Briefly mention what you did in your report. Make sure to do this in the first few minutes of class (or earlier).

To check the limit of the number of processes, run ```ulimit``` command:

```
mingwei@duckpi02 ~ $ ulimit -u
7349
```

To change the maximum number of processes for all the users to 100, we can modify the file ```/etc/security/limits.conf```

```
*           soft    nproc          100
*           hard    nproc          100
```

After reboot, we can check the limit again and see if it changed to 100:

```
mingwei@duckpi02 ~ $ ulimit -u
100
```

## 2

This must be done in class. Make sure everyone has completed problem 1. Pick a victim Pi (not yours) and log in as a regular user.

* Try to bring it down, e.g., by using a [fork bomb](http://en.wikipedia.org/wiki/Fork_bomb).
* Check if your Pi is being attacked and by whom. Do you observe any problems? How did you deal with them (use sudo if you need to change other users' processes or files)?

I use the following forkbomb (more readable than the one-line version):

```bash
bomb() {
  bomb | bomb &
};
bomb
```

Once I start this process as myself, my user account is practically useless because I cannot run any other processes.
It forkbomb procedure will just keep trying to fork itself, and here is the output that has been repeatly showing on screen:

```
...
./forkbomb.sh: fork: retry: Resource temporarily unavailable
./forkbomb.sh: fork: retry: No child processes
./forkbomb.sh: fork: retry: No child processes
./forkbomb.sh: fork: retry: No child processes
./forkbomb.sh: fork: retry: No child processes
./forkbomb.sh: fork: retry: Resource temporarily unavailable
./forkbomb.sh: fork: retry: No child processes
./forkbomb.sh: fork: retry: Resource temporarily unavailable
...
```

To stop the attack, I need to login to the pi's other admin account and work on stop the processes. First we can learn how many processes the user is using:

```
pi@duckpi02 ~ $ ps aux |grep -e "^mingwei"|wc -l
100
```

Apparently, my account has already reached the upper limit of the system's requirement. That's why I cannot do anything else on this user account.

The next step will be stop the processes. Note that I cannot simply kill all the processes. Because once a process is killed, the other forkbomb will fork another process the fill the space, and the race will not stop. 
Therefore, we should first **stop** the processes, or "freeze" them, and then kill them all.

Use the following command, we can stop the processes: ```killall -STOP forkbomb.sh```. Then we can take a look at the status of the processes using ```ps aux```:


```
mingwei   6689  0.0  0.1   4344  1560 ?        T    10:54   0:00 /bin/bash ./forkbomb.sh
mingwei   6691  0.0  0.1   4368  1608 ?        T    10:54   0:00 /bin/bash ./forkbomb.sh
mingwei   6693  0.0  0.1   4368  1608 ?        T    10:54   0:00 /bin/bash ./forkbomb.sh
```

See that the processes are all in status ```T```, meaning that they were stopped and nolonger active.

Then we can kill all the processes using ```killall -KILL forkbomb.sh```. After that we can check how many processes the user still have:

```
pi@duckpi02 ~ $ ps aux |grep -e "^mingwei"|wc -l
0
```

I've also tried that on duckpi08.

## 3

Write a daemon (bash or any other language you prefer) that estimates what percentage of time your Pi was up and accessible over the network. Do this as early as possible. Note that just calling uptime is not quite sufficient and you may need to run scripts on other machines. Describe how you did this in your report. Make the automatically generated results available on a regular basis at some URL (e.g., in your ~/public_html space on ix) and post the URL on Piazza as soon as it's ready. For example, a basic plain text log file with regular periods, e.g., one for each hour and the % availability during each period would be fine, but the format is entirely up to you. Summary statistics would also be nice (e.g., per day and per week). The owner(s) of the Pi that was most accessible will win a surprise.

It was hen I almost finshed this question when I found that I understood the question wrong.
I was thinking that we should build a monitor to probe all the **other** Pi to see if they're up or not.
Accroding to my understanding, I wrote the following Java code as a service to collect all the information from the probing:

[my status keeper on bitbucket](https://bitbucket.org/mingwei/status_keeper)

Then I wrote the following script to periodically (**every ten minutes**) probe all the Pi machines and update the result to the Java server:

```
#!/bin/bash

for i in {1..8}
do
    ssh -o ConnectTimeout=5 mingwei@duckpi0$i exit
    if [ $? -eq 0 ]
    then
    	 ## successful
        curl $MON:4567/update/duckpi0${i}/0
    else
    	 ## failed
        curl $MON:4567/update/duckpi0${i}/1
    fi
done
```

There are now two reporting links: 

[full report](http://cougar.cs.uoregon.edu:4567/fullstatus) and [brief report](http://cougar.cs.uoregon.edu:4567/status)

Full report has all the probe results, while the brief reports only show the most recent machine status (UP or DOWN) and the percentage of successful probes.

![](http://i.imgur.com/bukLjAM.png)

![](http://i.imgur.com/V8G4KTN.png)


## 4

Log into other students' Pis and create various problems -- circular symbolic links, deleted open files, a daemon-like program that makes a lot of directories or files, creating hidden directories (e.g., named ...), etc. Be creative. Do NOT tell the Pi owners what you did. Do this on at least four other Pis (not your own). Write scripts for at least some of your mischief, so that you can easily replicate it on several machines. Note in your report what you did and to which Pis. Also let me know on Piazza which Pis you targeted (post private to instructors only!).

I noticed that duckpi01 does not enable ulimit, so I forkbombed it.

I obtained the root access on duckpi07, on which the default Pi password was still used. Theoretically, I could destroy the pi now with ```sudo rm -rf /```. But I figured, this is too harsh. So I decided to consume all the filesystem left space.

Specifically, I first changed user. Then I want to create super large files. After a bit of searching, I found out that using ```fallocate``` is the fastest way to do such a job:

```
USER@duckpi07 ~ $ fallocate -l 40G largefile
fallocate: largefile: fallocate failed: No space left on device
USER@duckpi07 ~ $ df
Filesystem     1K-blocks     Used Available Use% Mounted on
rootfs          29094896 27593920         0 100% /
/dev/root       29094896 27593920         0 100% /
devtmpfs          470368        0    470368   0% /dev
tmpfs              94936      256     94680   1% /run
tmpfs               5120        0      5120   0% /run/lock
tmpfs             189860        0    189860   0% /run/shm
/dev/mmcblk0p6     60479    14542     45938  25% /boot
/dev/mmcblk0p5    499656      408    462552   1% /media/data
/dev/mmcblk0p3     27633      397     24943   2% /media/SETTINGS
```

Allocating a 40G file takes no more than 1 second, which is far more efficient than using ```dd```. 

However, by doing so, people can still find out that the file is created by me because I am the only person logged in during the time when the file was created. To prevent anyone from finding out such information, I also change the creation time of the file using ```touch```:

```
mingwei@duckpi07 /home/liuly $ sudo touch -d 19700101 largefile
mingwei@duckpi07 /home/liuly $ ls -l
total 0
-rw-r--r-- 1 root  root   0 Jan  1  1970 largefile
lrwxrwxrwx 1 liuly liuly 44 Jan  1  1970 pistore.desktop -> /usr/share/indiecity/pistore/pistore.desktop
```

We can see that the largefile is shown created on Unix time 0.


## 5

On your own Pi, write scripts to detect as many problems as you can and fix them automatically if possible or at least report them (e.g., to standard out). You should run these with sudo or as root. On Monday, May 4, we will go through the reports to see what problems on your Pi you were able to find and which ones you missed.

Since during the class time, the ```brix``` server was down, casued by Adam's forkbomb, our duckpi02 is the only accessible machine during the time period. 
There were multiple attempts on breaking our system, while we survived them all. 
Here I will just list some of the attempts and how we dealt with them.

### wall flood

Liyuan has conducted a wall flood that effectively disrupt our usage of our pi.

To stop it, I removed the permissions of the ```wall``` program:

```
sudo chmod -r -x /usr/bin/wall
```

Note that we also removed the read permission in order to prevent anyone from copy the program to local folder and run it. But this does not prevent users from download them from the Internet

### write flood

Adam use the ```write``` command to flood our terminal sessions.

Similarly, we removed the permissions for this command as well.

### extreme memory usage 

User liuly put a c code on its home folder which runs a memory allocation function in a while(1) loop. As a result, we need to reboot the computer and limit users memory usage by changing the ```/etc/security/limits.conf``` file.

### my limits.conf

Here is my configuration for limiting all the users I created. They are all in the group ```snal```:

```
*           soft    nproc          100
*           hard    nproc          100
@snal   hard    as      10000
@snal   hard    memlock 10000
@snal   hard    rss     10000
@snal   hard    stack   10000
@snal   hard    cpu             1
@snal   hard    maxlogins       3
@snal   hard    fsize   10000
@snal   hard    nofile  10
@snal   hard    locks   10
@snal   hard    data    10000
```

This can effectively stop all the destructive attempts on my Pi.

I also fixed all the files with 0777 permission

``` sudo find / -type f \( -perm -o+w \) -exec chmod o-w {} \;```


## 6

Graduate students only (optional for undergrads): Extend problem 3 to alert you if any problems are detected -- e.g., Pi can't be reached, or the load is over some threshold, etc.

I added the following line to my script's connection failure part to email me if my Pi is not accessible:

```
echo "cannot reach duckpi02" | \
mail -s "duckpi02 connection allert" mingwei@cs.uoregon.edu
```
