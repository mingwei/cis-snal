# Week 8 report

Virtual machine setup:

![](http://i.imgur.com/eTgL0Dn.png)

## DHCP

### Setup

*The goal of this exercise is to learn about using and configuring a DHCP
server. Instead of assigning a static IP address to the ethernet interface of your Pi (as
you did in the previous exercise), the address should be assigned by your DHCP
server that is running on a node attached to the ethernet subnet. Install a DHCP
server (the dhcpd program) on your Pi and test its basic functionality by attaching two
nodes to the same subnet. The DHCP server should also provide the information for
the DNS server and default gateway of the network to the client nodes. Coordinate
with you classmate so that a few of you attach your Pis to the same subnet (the
ethernet switch that we have in Boyana's office now), and have the gateway routing
feature (from the previous exercise) enabled on at least on of Pis, say x, then x can be
used as the default gateway. Use tcpdump to capture the exchanged messages
between your client and the DHCP servers, and check the content of those
messages. In your report describe the type of exchanged messages between a client
and a DHCP server, the content of these messages, and specify the main pieces of
information in each message (e.g. type of ICMP message, MAC addresses of the
client and server, provided IP address by the server, …)*

Followed this [link](http://www.tecmint.com/install-and-configure-multihomed-isc-dhcp-server-on-debian-linux/) to set up the ISC-DHCP server.

### Test

Disable VirtualBox built-in DHCP server:

```
VBoxManage list dhcpservers
VBoxManage dhcpserver remove --netname HostInterfaceNetworking-vboxnet0
```

Remove the IP settings on the client machine:

```
sudo ip addr flush dev eth1
```

Then send a DHCP request to the network:

```
vagrant@precise32:~$ sudo dhclient -v eth1
Internet Systems Consortium DHCP Client 4.1-ESV-R4
Copyright 2004-2011 Internet Systems Consortium.
All rights reserved.
For info, please visit https://www.isc.org/software/dhcp/

Listening on LPF/eth1/08:00:27:0a:8c:1c
Sending on   LPF/eth1/08:00:27:0a:8c:1c
Sending on   Socket/fallback
DHCPREQUEST of 192.168.0.10 on eth1 to 255.255.255.255 port 67
DHCPACK of 192.168.0.10 from 192.168.0.102
bound to 192.168.0.10 -- renewal in 271 seconds.
```

### Capture the requests

```
vagrant@precise32:~$ sudo tcpdump -nli eth1
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on eth1, link-type EN10MB (Ethernet), capture size 65535 bytes
00:52:26.714262 IP 192.168.0.1.17500 > 192.168.0.255.17500: UDP, length 180
00:52:34.998463 IP 0.0.0.0.68 > 255.255.255.255.67: BOOTP/DHCP, Request from 08:00:27:0a:8c:1c, length 300
00:52:34.998630 IP 192.168.0.102 > 192.168.0.10: ICMP echo request, id 4136, seq 0, length 28
00:52:35.999702 IP 192.168.0.102.67 > 192.168.0.10.68: BOOTP/DHCP, Reply, length 300
00:52:36.000927 IP 0.0.0.0.68 > 255.255.255.255.67: BOOTP/DHCP, Request from 08:00:27:0a:8c:1c, length 300
00:52:36.002041 IP 192.168.0.102.67 > 192.168.0.10.68: BOOTP/DHCP, Reply, length 300
```

In this case, we can see that the request first sent from 0.0.0.0, and our DHCP server (192.168.0.102) replied and gave the client an IP address 192.168.0.10.

### Two DHCP servers

*In practice, there could be settings where multiple DHCP servers observe the same
group of clients and can provide them with IP address (and other information). For
example, if you all connect your Pis to the same ethernet segment and run a DHCP
server on multiple Pis, there will be a conflict among theses servers for assigning IP
address to a client on that subnet. The design of the DHCP protocol accommodates
such setting and ensures that only one of the DHCP servers assigns an IP address to
a client. Using tcpdump capture the exchanged messages between a client and 2 (or
more) DHCP servers that run on the same subnet to explain how one (of multiple)
DHCP server(s) is selected to serve the client.
Another problem in a setting with multiple visible DHCP servers to a client is to ensure
that the assigned IP addresses by each DHCP server is unique. Consider a
subnet with the assigned  IP prefix of 192.168.15.0/24. If all the DHCP servers on the
same subnet manage the same prefix, while at each point only one of them provides
IP address to a client, it is still possible that a single IP address is handed out by two
DHCP servers to different clients if the book keeping of the assigned addresses by
different servers become inconsistent (this problem may not occur in certain OSes
such as Linux where the client uses ARP ping before accepting a proposed IP
address). Given a specific set of DHCP servers, you should devise a solution for this
problem and show that it works in practice with multiple DHCP servers on the same
subnet while fully utilizing all the addresses in the provided prefix. Hint: You should
make adjustments to the configuration file for DHCP servers (at /etc/dhcpd.conf) to
achieve this goal.*

Capture from one the client:

```
vagrant@precise32:~$ sudo tcpdump -nli eth1
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on eth1, link-type EN10MB (Ethernet), capture size 65535 bytes
01:09:41.203338 IP 0.0.0.0.68 > 255.255.255.255.67: BOOTP/DHCP, Request from 08:00:27:0a:8c:1c, length 300
01:09:41.204646 IP 192.168.0.102.67 > 192.168.0.10.68: BOOTP/DHCP, Reply, length 300
01:09:41.204662 IP 192.168.0.103.67 > 192.168.0.10.68: BOOTP/DHCP, Reply, length 300
```

Results from the client:

```
vagrant@precise32:~$ sudo dhclient -v eth1
Internet Systems Consortium DHCP Client 4.1-ESV-R4
Copyright 2004-2011 Internet Systems Consortium.
All rights reserved.
For info, please visit https://www.isc.org/software/dhcp/

Listening on LPF/eth1/08:00:27:0a:8c:1c
Sending on   LPF/eth1/08:00:27:0a:8c:1c
Sending on   Socket/fallback
DHCPREQUEST of 192.168.0.10 on eth1 to 255.255.255.255 port 67
DHCPACK of 192.168.0.10 from 192.168.0.102
bound to 192.168.0.10 -- renewal in 260 seconds.
```

We can see that from the capture, we saw two replies from both DHCP servers (.102 and .103), however, the client only accepts the result from .102.

The DHCP client will simply accept **the first DHCP offer** and thus avoid any conflict.

### DHCP Server Ranges

DHCP servers can each takes care of one range of the IP addresses without cocflicting with each other. In the configuration file ```/etc/dhcp/dhcpd.conf```, we can set the following for each to split a range of IP address block into two:

```
subnet 192.168.0.0 netmask 255.255.255.0 {
  range 192.168.0.11 192.168.0.20;
  option routers 192.168.0.100;
}

subnet 192.168.0.0 netmask 255.255.255.0 {
  range 192.168.0.21 192.168.0.30;
  option routers 192.168.0.100;
}
```


### MAC Spoof Attack on DHCP Server

*The goal of this exercise is to create a misbehaving node that obtains all IP
addresses that are managed by a single DHCP server. Using the ip command from
the previous exercise, you can assign a virtual MAC address to your node. Since
DHCP servers identify and assign an IP address to each node on a subnet based on
its MAC address, a node with multiple MAC address can request and obtain a
separate IP address for each MAC address. Using this idea, develop a small script
that consumes all the IP addresses of a single DHCP server.*

I followed this [article](http://linuxconfig.org/change-mac-address-with-macchanger-linux-command) to enable automatic changing MAC addresses.

```
ip addr flush dev eth1
ifconfig eth1 down
macchanger -r eth1
ifconfig eth1 up
dhclient -v eth1
```

I got DHCPNAK replies first:

```
DHCPREQUEST of 192.168.0.10 on eth1 to 255.255.255.255 port 67
DHCPNAK from 192.168.0.102
```

Then I realized that the random MAC may not work:

```
ip addr flush dev eth1
ifconfig eth1 down
macchanger -m 08:00:27:0a:8c:1d eth1
ifconfig eth1 up
dhclient -v eth1
```

The failure may be caused by the requirement on MAC address and DHCP client ID.
Refer to this [article](https://www.net.princeton.edu/announcements/dhcp-cliid-must-match-chaddr.html)

**Problem:** the newly assigned MAC address was not recognized by virtualbox, as a result the "switch" functionality provided by virtualbox does work perfectly in this case.

**Conclusion:** the DHCP spoofing attack cannot be repeated on VirtualBox environment.



***

## DNS

### Set up DNS server

*DNS servers perform the important tasks of translating a domain name
(www.cnn.com) into an IP address among other things. In this section, you conduct a
few exercises with the DNS servers. First, you need to become familiar with
the BIND program that implements a DNS server for Unix systems by the ISC. You
should first become familiar with the syntax of zone files (DNS records)
that BIND uses. A good resource on this subject is the following link:<https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/3/html/Reference_Guide/s1-bind-zone.html>.*

*Then you should configure two types of entry, namely an A and PTR entry. The first
type of entry is used for forward address resolution (translation of a DNS name to IP
address) while the second is used in reverse DNS look up. Use the BIND program to
insert DNS records of various types for different domains (e.g. SNAIL.cs.uoregon,edu)
to your DNS server. Turn in a tar file of all your zone files with your project report.*

I followed this [instruction](http://www.tecmint.com/install-dns-server-in-ubuntu-14-04/) for this problem.

Install bind related softwares:

```
sudo apt-get install bind9 dnsutils -y
```

Then set up DNS request forwarding to use the VirtualBox's DNS server at `/etc/bind/named.conf.options`.

Then I created one zone ```snail.cs.uoregon.edu``` (the zone files are in the repository for week 8), and dig it at localhost:

```
vagrant@precise32:/etc/bind$ dig @127.0.0.1 snail.cs.uoregon.edu

; <<>> DiG 9.8.1-P1 <<>> @127.0.0.1 snail.cs.uoregon.edu
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 49244
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 1

;; QUESTION SECTION:
;snail.cs.uoregon.edu.          IN      A

;; ANSWER SECTION:
snail.cs.uoregon.edu.   604800  IN      A       192.168.0.105

;; AUTHORITY SECTION:
snail.cs.uoregon.edu.   604800  IN      NS      ns.snail.cs.uoregon.edu.

;; ADDITIONAL SECTION:
ns.snail.cs.uoregon.edu. 604800 IN      A       192.168.0.105

;; Query time: 0 msec
;; SERVER: 127.0.0.1#53(127.0.0.1)
;; WHEN: Thu May 21 03:23:34 2015
;; MSG SIZE  rcvd: 87
```

### Dig command

*The dig command is used to query a DNS server for resolution or
reverse name resolution. Practice with this command and its features. You can use dig
to check whether the records that you added to your own DNS server are accessible
and provide correct information. You can add a fake entry for a web site
( <http://www.amazon.com> ) to your DNS server and specify your own IP address so any query
for that web site is directed to your node. Given that it is so easy to add a fake record
to DNS and highjacking a web site, investigate and explain how this problem is
avoided in practice.*

I added one more zone for amazonc.om, and dig it through my own DNS server. Here is the result:

```
vagrant@precise32:/etc/bind$ dig @127.0.0.1 www.amazon.com

; <<>> DiG 9.8.1-P1 <<>> @127.0.0.1 www.amazon.com
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 27793
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 1

;; QUESTION SECTION:
;www.amazon.com.                        IN      A

;; ANSWER SECTION:
www.amazon.com.         604800  IN      A       192.168.0.105

;; AUTHORITY SECTION:
amazon.com.             604800  IN      NS      ns.amazon.com.

;; ADDITIONAL SECTION:
ns.amazon.com.          604800  IN      A       192.168.0.105

;; Query time: 0 msec
;; SERVER: 127.0.0.1#53(127.0.0.1)
;; WHEN: Thu May 21 03:39:34 2015
;; MSG SIZE  rcvd: 81
```

Because of the hierachy organization of the DNS servers, the company's name server is maintained by the upstream DNS provider.
For example, the .COM TLD will maintain the records for amazon.com.
It is easy to mess up with the records if you have access to those TLD servers.
However, it is obvious that it is extrememly hard to get hold of such top level servers.
As long as the parent domain (in previous example, the .com TLD) is secure, the DNS request will be forwarded to the comany's domain itself. 
No third party could easily modify the records on their servers, thus it is hard to conduct malicious activities.

### DNS query latency measurement

*Your DNS server obviously can not resolve any query for the names (google.com) or
IP addresses that it does not know about. In these cases, your server should be
configured to forward the request to an external DNS server. Such a forwarding can
be done in a recursive or iterative manner (one of the configuration choices).
Configure your server to forward request for any missing record to UO DNS server
and practice with both (recursive and iterative) forwarding options. Then, consider the
provided list of 500 URLs (that represent some of the most popular URL
over the Internet) and query your server to resolve these URL. These queries should
be all forwarded to the UO DNS server. Use DNS performance tools such as dnsperf 
to measure the resolution latency of your server for each query and plot the
Cumulative Distribution Function (CDF) of these latency values. Note that the DNS
servers cache a missing record that they obtain for a request which in turn
improves the response time for the next request for the same record. Repeat your
queries for some of the 500 URLs and check produce the CDF of resolution latency
values for this second round. Explain whether and how much the look up performance
has improved as a result of caching.*

DNS service information of the University of Oregon can be found at [here](https://it.uoregon.edu/nts/useful-host-info).

Install ```dnsperf``` ([source](http://snobbycloud.blogspot.com/2012/07/installing-dnsperf-to-ubuntu-1204.html)]:

```
sudo apt-get install libbind-dev build-essential
sudo apt-get install dnsutils bind9
sudo apt-get install libcap-dev tshark
sudo apt-get install libxml2-dev
sudo apt-get install libssl-dev
sudo apt-get install libcap-dev
wget ftp://ftp.isc.org/isc/bind9/9.9.0/bind-9.9.0.tar.gz
tar zxvf bind-9.9.0.tar.gz
sudo cp -p bind-9.9.0/lib/isc/include/isc/hmacsha.h /usr/include/isc/
sudo ln -s /usr/lib/i386-linux-gnu/libgssapi_krb5.so.2.2 /usr/lib/i386-linux-gnu/libgssapi_krb5.so
wget ftp://ftp.nominum.com/pub/nominum/dnsperf/2.0.0.0/dnsperf-src-2.0.0.0-1.tar.gz
tar zxvf dnsperf-src-2.0.0.0-1.tar.gz
cd dnsperf-src-2.0.0.0-1/
./configure
make
sudo make install
```

First, we need to process the input into the required format for dnsperf.
The format consists of two parts for each line of input: the domain name in interest, and the record type. Here is an example.

```
www.amazon.com A
```

I removed the `http://` prefix in the input, and added `A` to the end of every line.

Then we can run the dnsperf to get our result. 
Note that since we are going to query a large amount of domains, we need to slowdown the speed, otherwise the network operators may block our prober.

```
dnsperf -d AlexaNoRank.txt -Q 50 -s 127.0.0.1 -t 1 -v
```

Here is the explanation of the options:

* `-d`: the input file
* `-Q`: the maximum number of requests per second
* `-s`: the server (here we use localhost)
* `-t`: maximum waiting time
* `-v`: print out the latency result for each request (important for drawing CDF)


Another thing that we need to do is to flush all the existing cached entries in our local DNS server:

```
sudo service bind9 restart
```

First round:

```
Statistics:

  Queries sent:         500
  Queries completed:    486 (97.20%)
  Queries lost:         14 (2.80%)

  Response codes:       NOERROR 476 (97.94%), NXDOMAIN 10 (2.06%)
  Average packet size:  request 33, response 298
  Run time (s):         10.326492
  Queries per second:   47.063417

  Average Latency (s):  0.124188 (min 0.001564, max 0.963098)
  Latency StdDev (s):   0.170860
```

Second round:

```
Statistics:

  Queries sent:         500
  Queries completed:    495 (99.00%)
  Queries lost:         5 (1.00%)

  Response codes:       NOERROR 485 (97.98%), NXDOMAIN 10 (2.02%)
  Average packet size:  request 33, response 302
  Run time (s):         10.002155
  Queries per second:   49.489335

  Average Latency (s):  0.014495 (min 0.000074, max 0.855494)
  Latency StdDev (s):   0.065955
```

From this summarized result, we can see that the second round of requests took around 10% of the time comparing to the first round.
This indicates that the local cache did kick in and boost the performance of the DNS queries.

The CDF figure is shown below:

![](dns_latency.png)

NOTE: the CDF axis is on the bottom.

From the result figure, we can see that the DNS request latency with cached entries is much less than without cache entries.
This confirms the expectation of caching's effectiveness.