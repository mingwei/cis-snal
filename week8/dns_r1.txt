DNS Performance Testing Tool
Nominum Version 2.0.0.0

[Status] Command line: dnsperf -d AlexaNoRank.txt -Q 50 -s 127.0.0.1 -t 1 -v
[Status] Sending queries (to 127.0.0.1)
[Status] Started at: Thu May 21 05:01:25 2015
[Status] Stopping after 1 run through file
> NOERROR www.Google.com  A 0.005358
> NOERROR www.Facebook.com  A 0.002861
> NOERROR www.Youtube.com  A 0.003061
> NOERROR www.Yahoo.com  A 0.002853
> NOERROR www.Baidu.com  A 0.004535
> NOERROR www.Amazon.com  A 0.002168
> NOERROR www.Twitter.com  A 0.002813
> NOERROR www.Qq.com  A 0.008519
> NOERROR www.Taobao.com  A 0.058515
> NOERROR www.Google.co.in  A 0.026544
> NOERROR www.Linkedin.com  A 0.003500
> NOERROR www.Live.com  A 0.050394
> NOERROR www.Sina.com.cn  A 0.013635
> NOERROR www.Wikipedia.org  A 0.176372
> NOERROR www.Tmall.com  A 0.002104
> NOERROR www.T.co  A 0.005301
> NOERROR www.Blogspot.com  A 0.030562
> NOERROR www.Ebay.com  A 0.062803
> NOERROR www.Google.de  A 0.018207
> NOERROR www.Hao123.com  A 0.003448
> NOERROR www.Google.co.jp  A 0.136481
> NOERROR www.Bing.com  A 0.002807
> NOERROR www.Reddit.com  A 0.002042
> NOERROR www.Google.co.uk  A 0.006078
> NOERROR www.Msn.com  A 0.004775
> NOERROR www.Amazon.co.jp  A 0.008557
> NOERROR www.Yahoo.co.jp  A 0.291733
> NOERROR www.Yandex.ru  A 0.157453
> NOERROR www.Instagram.com  A 0.002829
> NOERROR www.Google.com.br  A 0.040253
> NOERROR www.Google.fr  A 0.018870
> NOERROR www.Tumblr.com  A 0.001939
> NOERROR www.Wordpress.com  A 0.002741
> NOERROR www.Imgur.com  A 0.002605
> NOERROR www.Pinterest.com  A 0.002714
> NOERROR www.Weibo.com  A 0.475937
> NOERROR www.Sohu.com  A 0.115752
> NOERROR www.Paypal.com  A 0.063458
> NOERROR www.Apple.com  A 0.004711
> NOERROR www.Vk.com  A 0.346118
> NOERROR www.Microsoft.com  A 0.004620
> NOERROR www.Ask.com  A 0.059635
> NOERROR www.Aliexpress.com  A 0.105392
> NOERROR www.Xvideos.com  A 0.123054
> NOERROR www.Onclickads.net  A 0.044341
> NOERROR www.Gmail.com  A 0.003704
> NOERROR www.Imdb.com  A 0.003018
> NOERROR www.Google.ru  A 0.072807
> NOERROR www.Google.it  A 0.072071
> NOERROR www.Adcash.com  A 0.014229
> NOERROR www.Alibaba.com  A 0.055411
> NOERROR www.Fc2.com  A 0.050192
> NOERROR www.Google.es  A 0.016620
> NOERROR www.Amazon.de  A 0.008709
> NOERROR www.Mail.ru  A 0.237610
> NOERROR www.Netflix.com  A 0.002863
> NOERROR www.Googleadservices.com  A 0.004079
> NOERROR www.Stackoverflow.com  A 0.053970
> NOERROR www.Craigslist.org  A 0.002901
> NOERROR www.Google.ca  A 0.017472
> NOERROR www.Xhamster.com  A 0.009132
> NOERROR www.Go.com  A 0.054129
> NOERROR www.Google.com.hk  A 0.015067
> NOERROR www.Tianya.cn  A 0.002073
> NOERROR www.Naver.com  A 0.048031
> NOERROR www.Diply.com  A 0.150083
> NOERROR www.163.com  A 0.006136
> NOERROR www.Google.com.tr  A 0.016907
> NOERROR www.Bbc.co.uk  A 0.058777
> NOERROR www.Pornhub.com  A 0.034258
> NOERROR www.Adobe.com  A 0.002911
> NOERROR www.Amazon.co.uk  A 0.080960
> NOERROR www.Gmw.cn  A 0.207347
> NOERROR www.Amazon.cn  A 0.002453
> NOERROR www.Google.com.mx  A 0.023677
> NOERROR www.Kickass.to  A 0.195407
> NOERROR www.Dropbox.com  A 0.003827
> NOERROR www.Rakuten.co.jp  A 0.143130
> NOERROR www.Ebay.de  A 0.127702
> NOERROR www.Google.pl  A 0.049657
> NOERROR www.Cnn.com  A 0.108408
> NOERROR www.Ok.ru  A 0.232984
> NOERROR www.Googleusercontent.com  A 0.003208
> NOERROR www.Nicovideo.jp  A 0.161546
> NOERROR www.Jd.com  A 0.009827
> NOERROR www.Github.com  A 0.053074
> NOERROR www.Dailymotion.com  A 0.204827
> NOERROR www.Espn.go.com  A 0.167505
> NOERROR www.Youku.com  A 0.010526
> NOERROR www.Google.com.au  A 0.213628
> NOERROR www.Outbrain.com  A 0.104656
> NOERROR www.Directrev.com  A 0.089478
> NOERROR www.Google.co.id  A 0.025230
> NOERROR www.Flipkart.com  A 0.097894
> NOERROR www.Buzzfeed.com  A 0.004222
> NOERROR www.Cntv.cn  A 0.336841
> NOERROR www.Pixnet.net  A 0.158701
> T www.360.cn  A
> NOERROR www.Blogger.com  A 0.054998
> NOERROR www.Wikia.com  A 0.003184
> NOERROR www.Huffingtonpost.com  A 0.004180
> NOERROR www.Google.co.kr  A 0.195356
> NOERROR www.Soso.com  A 0.446346
> NOERROR www.Livedoor.com  A 0.014083
> NOERROR www.Ebay.co.uk  A 0.128426
> NOERROR www.Indiatimes.com  A 0.014888
> NOERROR www.Amazon.in  A 0.009455
> NOERROR www.Google.com.tw  A 0.051932
> NOERROR www.People.com.cn  A 0.474129
> NOERROR www.Blogspot.in  A 0.002296
> NOERROR www.Google.com.eg  A 0.015960
> NOERROR www.Amazonaws.com  A 0.069912
> NOERROR www.Chinadaily.com.cn  A 0.433436
> NOERROR www.Dailymail.co.uk  A 0.011099
> NOERROR www.Xinhuanet.com  A 0.015558
> NOERROR www.Flickr.com  A 0.004469
> NOERROR www.Tudou.com  A 0.196662
> NOERROR www.Booking.com  A 0.368390
> NOERROR www.Chase.com  A 0.222546
> NOERROR www.Wordpress.org  A 0.009793
> NOERROR www.Yelp.com  A 0.004169
> NOERROR www.Uol.com.br  A 0.558348
> NOERROR www.Ettoday.net  A 0.175958
> NOERROR www.China.com  A 0.410058
> NOERROR www.Coccoc.com  A 0.175723
> NOERROR www.About.com  A 0.004155
> NOERROR www.Globo.com  A 0.176051
> NOERROR www.Sogou.com  A 0.427698
> NOERROR www.Google.com.ar  A 0.017736
> NOERROR www.Google.nl  A 0.172528
> NOERROR www.Bankofamerica.com  A 0.134385
> NOERROR www.Bycontext.com  A 0.596784
> NOERROR www.Popads.net  A 0.165303
> NOERROR www.Douban.com  A 0.284229
> NOERROR www.Xnxx.com  A 0.427089
> NOERROR www.Ameblo.jp  A 0.087465
> NOERROR www.Godaddy.com  A 0.051141
> T www.Alipay.com  A
> NOERROR www.Youradexchange.com  A 0.113790
> NOERROR www.Bongacams.com  A 0.002428
> NOERROR www.Salesforce.com  A 0.105504
> NOERROR www.Themeforest.net  A 0.044473
> NOERROR www.Etsy.com  A 0.004460
> NOERROR www.Twitch.tv  A 0.067045
> T www.Nytimes.com  A
> NOERROR www.Pconline.com.cn  A 0.349195
> NOERROR www.Cnet.com  A 0.153658
> NOERROR www.Dmm.co.jp  A 0.202503
> NOERROR www.Slideshare.net  A 0.191392
> NOERROR www.Amazon.fr  A 0.009367
> NOERROR www.Redtube.com  A 0.071172
> NOERROR www.Daum.net  A 0.336803
> NOERROR www.Quora.com  A 0.005051
> NOERROR www.Deviantart.com  A 0.080905
> NXDOMAIN www.Warmportrait.com  A 0.009615
> NOERROR www.Loading-delivery1.com  A 0.130565
> NOERROR www.Google.com.pk  A 0.114969
> NOERROR www.Bbc.com  A 0.003136
> NOERROR www.Weather.com  A 0.038930
> NOERROR www.Adf.ly  A 0.054805
> NOERROR www.Ilividnewtab.com  A 0.081821
> NOERROR www.Google.com.sa  A 0.239110
> NOERROR www.Indeed.com  A 0.003620
> NOERROR www.Stackexchange.com  A 0.054758
> NOERROR www.Theguardian.com  A 0.254218
> NOERROR www.Stamplive.com  A 0.202362
> NOERROR www.Vimeo.com  A 0.086171
> NOERROR www.Snapdeal.com  A 0.018950
> NOERROR www.Walmart.com  A 0.044030
> NOERROR www.Youporn.com  A 0.228724
> NOERROR www.Naver.jp  A 0.155655
> NOERROR www.Soundcloud.com  A 0.003557
> NOERROR www.Forbes.com  A 0.209266
> NOERROR www.Microsoftonline.com  A 0.011233
> NOERROR www.Google.co.th  A 0.021136
> NXDOMAIN www.Bp.blogspot.com  A 0.016241
> NOERROR www.Wellsfargo.com  A 0.015823
> NOERROR www.Espncricinfo.com  A 0.009268
> NOERROR www.Reference.com  A 0.061171
> NOERROR www.Google.co.za  A 0.015452
> NOERROR www.Zillow.com  A 0.002319
> NOERROR www.Life.com.tw  A 0.280236
> NOERROR www.Aol.com  A 0.229636
> NOERROR www.Google.gr  A 0.046620
> NOERROR www.Amazon.it  A 0.007858
> NOERROR www.W3schools.com  A 0.010634
> NOERROR www.Feedly.com  A 0.052966
> NOERROR www.Mozilla.org  A 0.011373
> NOERROR www.Office365.com  A 0.003391
> NOERROR www.Wikihow.com  A 0.065468
> NOERROR www.Mailchimp.com  A 0.018181
> NOERROR www.Thepiratebay.se  A 0.055284
> NOERROR www.Tripadvisor.com  A 0.036556
> NOERROR www.Mystart.com  A 0.121839
> NOERROR www.Leboncoin.fr  A 0.183731
> NOERROR www.Google.com.ua  A 0.019931
> NOERROR www.Livejournal.com  A 0.082595
> NOERROR www.Businessinsider.com  A 0.003233
> NOERROR www.Livejasmin.com  A 0.002493
> NOERROR www.Ifeng.com  A 0.007212
> NOERROR www.Vice.com  A 0.085966
> NOERROR www.Allegro.pl  A 0.231393
> NOERROR www.Wikimedia.org  A 0.125768
> NOERROR www.Ikea.com  A 0.086779
> NOERROR www.Force.com  A 0.062838
> NOERROR www.Washingtonpost.com  A 0.004506
> NOERROR www.Mediafire.com  A 0.009158
> NOERROR www.Gamer.com.tw  A 0.032373
> NOERROR www.Zol.com.cn  A 0.314920
> NOERROR www.Google.be  A 0.050047
> NOERROR www.Taboola.com  A 0.003219
> NOERROR www.Files.wordpress.com  A 0.188492
> NOERROR www.Secureserver.net  A 0.098196
> NOERROR www.Onet.pl  A 0.304660
> NOERROR www.9gag.com  A 0.043415
> NOERROR www.Nih.gov  A 0.107653
> NOERROR www.Sourceforge.net  A 0.053642
> NOERROR www.Pixiv.net  A 0.301325
> NOERROR www.Foxnews.com  A 0.039417
> NOERROR www.Blogspot.com.br  A 0.022937
> NOERROR www.Google.com.co  A 0.143745
> NOERROR www.Web.de  A 0.099236
> NXDOMAIN www.Trklnks.com  A 0.281035
> NOERROR www.Blogfa.com  A 0.061424
> NOERROR www.Kakaku.com  A 0.548458
> NOERROR www.Intuit.com  A 0.136135
> NOERROR www.Google.com.sg  A 0.016866
> NOERROR www.Ups.com  A 0.007997
> NOERROR www.Theladbible.com  A 0.918826
> NOERROR www.Comcast.net  A 0.110946
> NOERROR www.Doublepimp.com  A 0.033837
> NOERROR www.Google.co.ve  A 0.187662
> NOERROR www.Popcash.net  A 0.855174
> NOERROR www.Bestbuy.com  A 0.204578
> NOERROR www.China.com.cn  A 0.928469
> NOERROR www.Google.cn  A 0.015639
> NOERROR www.Abs-cbnnews.com  A 0.058276
> NOERROR www.Avito.ru  A 0.032357
> NOERROR www.Americanexpress.com  A 0.012018
> NOERROR www.Usps.com  A 0.109887
> NOERROR www.Google.com.ng  A 0.016727
> NOERROR www.Google.ro  A 0.016795
> NOERROR www.Likes.com  A 0.162340
> NOERROR www.Office.com  A 0.011218
> NOERROR www.Zhihu.com  A 0.372512
> NOERROR www.Goo.ne.jp  A 0.140298
> NOERROR www.Target.com  A 0.073073
> NOERROR www.Doorblog.jp  A 0.034241
> NOERROR www.Gfycat.com  A 0.056010
> T www.Google.com.my  A
> NXDOMAIN www.Akamaihd.net  A 0.009585
> NOERROR www.Addthis.com  A 0.053727
> NOERROR www.Google.at  A 0.059193
> NOERROR www.Shutterstock.com  A 0.033719
> NOERROR www.Steamcommunity.com  A 0.031187
> NOERROR www.Gmx.net  A 0.296554
> NOERROR www.Ndtv.com  A 0.043204
> NOERROR www.Badoo.com  A 0.002580
> NOERROR www.Wix.com  A 0.269772
> NOERROR www.Homedepot.com  A 0.008993
> T www.Smzdm.com  A
> NOERROR www.Stumbleupon.com  A 0.002813
> NOERROR www.Skype.com  A 0.110333
> NOERROR www.Avg.com  A 0.025924
> NOERROR www.Archive.org  A 0.869451
> NOERROR www.Theadgateway.com  A 0.102343
> NOERROR www.Pandora.com  A 0.003882
> NOERROR www.Ign.com  A 0.050775
> NOERROR www.Amazon.es  A 0.034826
> NOERROR www.Weebly.com  A 0.228170
> NOERROR www.Telegraph.co.uk  A 0.010795
> NOERROR www.Mashable.com  A 0.009592
> T www.Xuite.net  A
> NOERROR www.Groupon.com  A 0.012440
> T www.Tubecup.com  A
> NOERROR www.Quikr.com  A 0.009099
> NOERROR www.Mercadolivre.com.br  A 0.022309
> NOERROR www.Dmm.com  A 0.148962
> NOERROR www.Softonic.com  A 0.329800
> NOERROR www.Goodreads.com  A 0.083089
> T www.Pclady.com.cn  A
> NOERROR www.Adplxmd.com  A 0.085597
> NOERROR www.Youm7.com  A 0.053191
> NOERROR www.Wordreference.com  A 0.085323
> NOERROR www.Hootsuite.com  A 0.287973
> NOERROR www.Ppomppu.co.kr  A 0.173461
> T www.Bitauto.com  A
> NOERROR www.Hdfcbank.com  A 0.059034
> NOERROR www.Bet365.com  A 0.009476
> NOERROR www.Bilibili.com  A 0.006787
> NOERROR www.Bild.de  A 0.267075
> NOERROR www.Github.io  A 0.088607
> NOERROR www.Wsj.com  A 0.009441
> NXDOMAIN www.Media.tumblr.com  A 0.050359
> NOERROR www.Google.se  A 0.200731
> NOERROR www.Spiegel.de  A 0.162193
> NOERROR www.Ask.fm  A 0.045250
> NOERROR www.Orange.fr  A 0.545359
> NOERROR www.Google.pt  A 0.252031
> NOERROR www.Bomb01.com  A 0.060003
> NOERROR www.Hulu.com  A 0.006123
> NOERROR www.Webmd.com  A 0.019572
> NOERROR www.Steampowered.com  A 0.083150
> NOERROR www.Google.com.pe  A 0.012985
> NOERROR www.Icicibank.com  A 0.307035
> NOERROR www.T-online.de  A 0.032588
> NOERROR www.Google.com.ph  A 0.016888
> NOERROR www.Engadget.com  A 0.181648
> NOERROR www.Wow.com  A 0.105107
> NOERROR www.Haosou.com  A 0.546317
> NOERROR www.Kaskus.co.id  A 0.235075
> NOERROR www.Pchome.net  A 0.313090
> NOERROR www.Jabong.com  A 0.097365
> NOERROR www.Webssearches.com  A 0.080942
> NOERROR www.Hurriyet.com.tr  A 0.001572
> NOERROR www.Spotify.com  A 0.177577
> NOERROR www.Rediff.com  A 0.097032
> NOERROR www.Uptodown.com  A 0.041283
> NOERROR www.Whatsapp.com  A 0.085888
> NOERROR www.Gome.com.cn  A 0.023620
> NOERROR www.Slickdeals.net  A 0.045961
> NOERROR www.Usatoday.com  A 0.027450
> NOERROR www.Hp.com  A 0.007236
> NOERROR www.Google.ch  A 0.020842
> NOERROR www.Trello.com  A 0.279525
> NOERROR www.Google.ae  A 0.025044
> NOERROR www.Tube8.com  A 0.010633
> NOERROR www.Udn.com  A 0.042179
> NOERROR www.Samsung.com  A 0.023048
> NOERROR www.Mobile01.com  A 0.011086
> NOERROR www.Caijing.com.cn  A 0.677847
> NOERROR www.Fedex.com  A 0.138212
> NOERROR www.Zendesk.com  A 0.058339
> NOERROR www.Rambler.ru  A 0.225739
> NOERROR www.Lifehacker.com  A 0.058515
> NOERROR www.Bloomberg.com  A 0.018009
> NOERROR www.Seznam.cz  A 0.357537
> NOERROR www.Teepr.com  A 0.061098
> NOERROR www.Fiverr.com  A 0.113553
> NOERROR www.Cbssports.com  A 0.242387
> NOERROR www.Speedtest.net  A 0.007401
> NOERROR www.Gameforge.com  A 0.204673
> NOERROR www.Bleacherreport.com  A 0.140379
> NOERROR www.Dell.com  A 0.008966
> NOERROR www.Chinatimes.com  A 0.404035
> NOERROR www.Answers.com  A 0.177585
> NOERROR www.Disqus.com  A 0.131905
> NXDOMAIN www.Truemediapipe.com  A 0.119077
> NOERROR www.Google.no  A 0.019788
> NOERROR www.Accuweather.com  A 0.015427
> NOERROR www.Amazon.ca  A 0.012497
> NOERROR www.Pcbaby.com.cn  A 0.237313
> NOERROR www.Evernote.com  A 0.011698
> NOERROR www.Wp.pl  A 0.437850
> NOERROR www.Extratorrent.cc  A 0.115354
> NOERROR www.Capitalone.com  A 0.157733
> NOERROR www.Ebay.in  A 0.021211
> NOERROR www.Rutracker.org  A 0.407131
> NOERROR www.Cloudfront.net  A 0.044379
> NOERROR www.Photobucket.com  A 0.005423
> NOERROR www.Iqiyi.com  A 0.006850
> NOERROR www.Kompas.com  A 0.114434
> NOERROR www.Mydomainadvisor.com  A 0.222070
> NOERROR www.Gsmarena.com  A 0.004247
> NOERROR www.Vipcpms.com  A 0.058292
> NOERROR www.Ijreview.com  A 0.963098
> NOERROR www.Gizmodo.com  A 0.004336
> NOERROR www.Mama.cn  A 0.620914
> NOERROR www.Newegg.com  A 0.142702
> NOERROR www.Torrentz.eu  A 0.254331
> NOERROR www.Trackingclick.net  A 0.063730
> NOERROR www.Techcrunch.com  A 0.006164
> NOERROR www.Google.com.bd  A 0.017546
> NOERROR www.Moz.com  A 0.229010
> NOERROR www.Hupu.com  A 0.612165
> NOERROR www.Ltn.com.tw  A 0.382517
> NOERROR www.Google.cz  A 0.200825
> NOERROR www.Libero.it  A 0.004457
> NOERROR www.Google.co.hu  A 0.026292
> NOERROR www.Webtretho.com  A 0.103104
> NOERROR www.Kickstarter.com  A 0.213516
> NOERROR www.Thefreedictionary.com  A 0.140572
> NOERROR www.Milliyet.com.tr  A 0.001826
> NOERROR www.Infusionsoft.com  A 0.105699
> NOERROR www.39.net  A 0.610628
> NOERROR www.Tistory.com  A 0.245959
> NOERROR www.Sahibinden.com  A 0.079895
> T www.Youth.cn  A
> NOERROR www.Codecanyon.net  A 0.123138
> NOERROR www.Staticwebdom.com  A 0.149931
> NOERROR www.Google.co.il  A 0.017715
> NOERROR www.Varzesh3.com  A 0.422501
> NXDOMAIN www.Twimg.com  A 0.055279
> NOERROR www.Bitly.com  A 0.005521
> NOERROR www.Justdial.com  A 0.061795
> NOERROR www.4shared.com  A 0.058762
> NOERROR www.Ameba.jp  A 0.045339
> NOERROR www.Eksisozluk.com  A 0.107940
> NOERROR www.Scribd.com  A 0.056296
> NOERROR www.Autohome.com.cn  A 0.381708
> NOERROR www.Googleapis.com  A 0.005555
> NOERROR www.Google.cl  A 0.036332
> NOERROR www.Verizonwireless.com  A 0.059879
> NOERROR www.Reuters.com  A 0.284648
> NOERROR www.Xcar.com.cn  A 0.023139
> NOERROR www.Oracle.com  A 0.014324
> NOERROR www.Babytree.com  A 0.053177
> NOERROR www.Meetup.com  A 0.862988
> NOERROR www.Surveymonkey.com  A 0.005341
> NOERROR www.Liputan6.com  A 0.077271
> NOERROR www.Mlb.com  A 0.004310
> NOERROR www.Hudong.com  A 0.237196
> NOERROR www.Xda-developers.com  A 0.139041
> NOERROR www.Paytm.com  A 0.095004
> NOERROR www.Att.com  A 0.085515
> NOERROR www.2ch.net  A 0.864143
> NOERROR www.Time.com  A 0.006050
> NOERROR www.Privatehomeclips.com  A 0.060739
> NOERROR www.Ptt01.cc  A 0.054909
> NOERROR www.Eastmoney.com  A 0.309915
> NOERROR www.Battle.net  A 0.125352
> NOERROR www.Sabah.com.tr  A 0.042566
> NOERROR www.Ebay.com.au  A 0.081274
> T www.Detik.com  A
> NOERROR www.Expedia.com  A 0.020089
> NOERROR www.Ero-advertising.com  A 0.048180
> T www.Irs.gov  A
> NOERROR www.Uploaded.net  A 0.060945
> NOERROR www.Macys.com  A 0.034539
> NOERROR www.Taleo.net  A 0.023945
> NOERROR www.Ck101.com  A 0.064430
> NOERROR www.Reimageplus.com  A 0.916072
> NOERROR www.Free.fr  A 0.004309
> NOERROR www.Liveinternet.ru  A 0.229195
> NOERROR www.Tlbb8.com  A 0.102677
> NOERROR www.Bhaskar.com  A 0.059678
> NOERROR www.Onlinesbi.com  A 0.888027
> NOERROR www.Lady8844.com  A 0.311904
> NOERROR www.Yandex.ua  A 0.157283
> NOERROR www.Blog.jp  A 0.034052
> NOERROR www.Ebay.it  A 0.059925
> T www.Ce.cn  A
> NOERROR www.32d1d3b9c.se  A 0.082399
> NOERROR www.Lazada.co.id  A 0.124342
> NOERROR www.Fbcdn.net  A 0.003905
> NOERROR www.Olx.in  A 0.082758
> NOERROR www.Blogspot.com.es  A 0.008601
> NOERROR www.List-manage.com  A 0.077779
> NOERROR www.Theverge.com  A 0.010048
> NOERROR www.Csdn.net  A 0.260271
> NOERROR www.Flirchi.com  A 0.052866
> NOERROR www.Repubblica.it  A 0.001564
> NOERROR www.Exoclick.com  A 0.055639
> NOERROR www.Google.ie  A 0.878414
> NOERROR www.Google.com.vn  A 0.018697
> NOERROR www.Nba.com  A 0.080715
> NOERROR www.Ink361.com  A 0.166564
> NOERROR www.Naukri.com  A 0.070046
> NOERROR www.Okcupid.com  A 0.054819
> NOERROR www.Chaturbate.com  A 0.033879
> NOERROR www.Warofclicks.com  A 0.022762
> NOERROR www.Rt.com  A 0.228212
> NOERROR www.Citibank.com  A 0.280748
> NOERROR www.Elpais.com  A 0.010160
> NOERROR www.Gap.com  A 0.011525
> NOERROR www.Retailmenot.com  A 0.157859
> NOERROR www.Mega.co.nz  A 0.263913
> NOERROR www.Icloud.com  A 0.011307
> NOERROR www.Albawabhnews.com  A 0.123217
> NOERROR www.Trulia.com  A 0.183602
> NOERROR www.Mi.com  A 0.077148
> NOERROR www.Glassdoor.com  A 0.003178
> NOERROR www.Gmarket.co.kr  A 0.329370
> NOERROR www.Livedoor.biz  A 0.275828
> NOERROR www.B5m.com  A 0.194681
> NXDOMAIN www.Wonderlandads.com  A 0.218658
> NOERROR www.Goal.com  A 0.289699
> NOERROR www.6pm.com  A 0.027539
> NXDOMAIN www.Bestadbid.com  A 0.052467
> NOERROR www.Xe.com  A 0.096270
> NOERROR www.Kayak.com  A 0.149322
> NOERROR www.Npr.org  A 0.110189
> NOERROR www.Hostgator.com  A 0.062483
> NOERROR www.Kouclo.com  A 0.047215
> NOERROR www.Rbc.ru  A 0.235134
> NOERROR www.Nyaa.se  A 0.856329
> NOERROR www.Buzzfil.net  A 0.065030
> NOERROR www.Blogimg.jp  A 0.033686
> NOERROR www.58.com  A 0.024582
> NOERROR www.Asana.com  A 0.046834
> NOERROR www.Starsports.com  A 0.098857
> NOERROR www.Ganji.com  A 0.339610
> NOERROR www.Hotels.com  A 0.044482
> NOERROR www.Mobile.de  A 0.350987
> NOERROR www.Google.dz  A 0.016635
> NOERROR www.Styletv.com.cn  A 0.016356
> T www.Xywy.com  A
> NXDOMAIN www.Ad132m.com  A 0.085669
> NOERROR www.Abril.com.br  A 0.234835
> NOERROR www.Marca.com  A 0.426320
[Status] Testing complete (end of file)

Statistics:

  Queries sent:         500
  Queries completed:    486 (97.20%)
  Queries lost:         14 (2.80%)

  Response codes:       NOERROR 476 (97.94%), NXDOMAIN 10 (2.06%)
  Average packet size:  request 33, response 298
  Run time (s):         10.326492
  Queries per second:   47.063417

  Average Latency (s):  0.124188 (min 0.001564, max 0.963098)
  Latency StdDev (s):   0.170860

