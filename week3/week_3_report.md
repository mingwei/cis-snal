# Week 3 Report

Mingwei Zhang

**Tip before the experiments:**
Back up your VM before doing these exercises, so you can recover quickly if you cannot boot into it after some of these exercises. You can also backup your Pi SD card to a disk image on your laptop or desktop (lots of examples on different OSs online), and I have some spares to use while you are recovering if you are in a hurry.


## 1. Questions about booting procedure

**Tasks:**

Answer the following questions about your virtual machine (or ix) **and** the Raspberry PI.

* What bootloader is used? What are its configuration files (i.e., full paths of config.txt and cmdline.txt or their equivalents)?
* What is the default runlevel?
* What programs start by default?

### Virtual Machine

The bootleader my virtual machine uses is GRUB. The config file is located at ```/boot/grub/grub.cfg```.

Since I boot up my virtual machine without graphical interface, the default runlevel is 2:

```
mingwei@hackvm:/boot/grub$ runlevel
N 2
```

At start, it runs the following commands, incurred by the scripts at ```/etc/rc2.d```.

```
mingwei@hackvm:~$ ls /etc/rc2.d/
README		   S20speech-dispatcher  S50saned	 S99ondemand
S20kerneloops	   S30vboxadd		 S70dns-clean	 S99rc.local
S20rsync	   S30vboxadd-x11	 S70pppd-dns
S20screen-cleanup  S35vboxadd-service	 S99grub-common
mingwei@hackvm:~$
```

### Pi

Raspberry Pi uses a special bootloader that specially designed for it, and take usages of GPU for booting procedure.

It runs at runlevel 2:

```
pi@duckpi02 /boot $ runlevel
N 2
```
At start, it runs the following commands, incurred by the scripts at ```/etc/rc2.d```.

```
pi@duckpi02 /etc/rc2.d $ ls
K05nfs-common  S01ifplugd       S02cron            S02ntp       S04rc.local
K05rpcbind     S01motd          S02dbus            S02rsync     S04rmnologin
README         S01rsyslog       S02dphys-swapfile  S02ssh
S01bootlogs    S01sudo          S02exim4           S03lightdm
S01cgroup-bin  S01triggerhappy  S02mdadm           S04plymouth
```

## 2. ```dmesg``` practice

**Tasks:**

In this exercise, you will practice using dmesg (see [examples][dmesg_example]) to check the status of the system. Use dmesg to look up the following on your VM or another real or virtual Ubuntu machine **and** on the Pi (and summarize in your report):

* The version of the Linux kernel
* The status of your WiFi connection
* The amount of available memory
* Number of USB devices (just the ports, not anything that may be plugged into them)

Note: Discovering the names of devices and other parts of the system in the system logs is part of the exercise.

[dmesg_example]: http://www.thegeekstuff.com/2010/10/dmesg-command-examples/

### Virtual Machine

Kernel 3.13.0-49-generic

```
[    0.000000] Linux version 3.13.0-49-generic (buildd@brownie) (gcc version 4.8.2 (Ubuntu 4.8.2-19ubuntu1) ) #81-Ubuntu SMP Tue Mar 24 19:29:48 UTC 2015 (Ubuntu 3.13.0-49.81-generic 3.13.11-ckt17)
[    0.000000] Command line: BOOT_IMAGE=/boot/vmlinuz-3.13.0-49-generic root=UUID=cb46122c-9d39-4633-b41c-c56cb7827468 ro quiet splash vt.handoff=7
```

It doesn't have WiFi on my virtual machine, so I show the Ethernet interface instead. The Ethernet port is ```eth0```, and it is up and ready to use according to the dmesg.
[source](http://www.linuxnix.com/2013/05/what-is-linuxunix-dmesg-command-and-how-to-use-it.html)

```
[    1.100499] e1000 0000:00:03.0 eth0: (PCI:33MHz:32-bit) 08:00:27:61:fb:7b
[    1.100510] e1000 0000:00:03.0 eth0: Intel(R) PRO/1000 Network Connection
[    3.361432] IPv6: ADDRCONF(NETDEV_UP): eth0: link is not ready
[    3.927079] IPv6: ADDRCONF(NETDEV_UP): eth0: link is not ready
[    3.927310] IPv6: ADDRCONF(NETDEV_UP): eth0: link is not ready
[    3.928518] e1000: eth0 NIC Link is Up 1000 Mbps Full Duplex, Flow Control: RX
[    3.928824] IPv6: ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready
```

The virtual machine has 4026396K memory available, which is roughly 4GB available. 
[source](http://unix.stackexchange.com/questions/30324/why-does-linux-show-both-more-and-less-memory-than-i-physically-have-installed)

```
[    0.000000] Memory: 4026396K/4193848K available (7386K kernel code, 1145K rwdata, 3408K rodata, 1340K init, 1444K bss, 167452K reserved)
```

Find out the number of USB devices is a little bit trickier, because of the nature of USB. After a little bit digging into the ```dmesg``` output, I found out that we can set for the following string to locate the new recognized USB port:
```
new usb device found
```

As a result, I've found out that there are 2 USB devices, one of which is a hub device that has 3 ports (```usb 1-1```):

```
mingwei@hackvm:~$ dmesg|grep -i "new usb device found"
[    0.578202] usb usb1: New USB device found, idVendor=1d6b, idProduct=0001
[    1.241627] usb 1-1: New USB device found, idVendor=80ee, idProduct=0021
[20319.754446] usb 1-1: New USB device found, idVendor=80ee, idProduct=0021
[21512.820321] usb 1-1: New USB device found, idVendor=80ee, idProduct=0021
```


### Pi

Kernel for my Pi is 3.18.7-v7+

```
[    0.000000] Linux version 3.18.7-v7+ (dc4@dc4-XPS13-9333) (gcc version 4.8.3 20140303 (prerelease) (crosstool-NG linaro-1.13.1+bzr2650 - Linaro GCC 2014.03) ) #755 SMP PREEMPT Thu Feb 12 17:20:48 GMT 2015
```

Because I unplugged the WiFi adapter, I will again use ```eth0``` as the target device. It works fine, apparently, with 100Mbps.

```
pi@duckpi02 /etc/rc2.d $ dmesg | grep -i eth0
[    3.444208] smsc95xx 1-1.1:1.0 eth0: register 'smsc95xx' at usb-bcm2708_usb-1.1, smsc95xx USB 2.0 Ethernet, b8:27:eb:0f:c1:e7
[   11.685570] smsc95xx 1-1.1:1.0 eth0: hardware isn't capable of remote wakeup
[   13.289049] smsc95xx 1-1.1:1.0 eth0: link up, 100Mbps, full-duplex, lpa 0xC1E1
```

Pi has 940740K memory available, around 1GB.

```
pi@duckpi02 /etc/rc2.d $ dmesg | grep Memory
[    0.000000] Memory policy: Data cache writealloc
[    0.000000] Memory: 940740K/966656K available (5785K kernel code, 377K rwdata, 1760K rodata, 396K init, 771K bss, 25916K reserved)
```

Pi has three USB ports:

```
pi@duckpi02 /etc/rc2.d $ dmesg|grep -i "new usb device found"
[    2.280269] usb usb1: New USB device found, idVendor=1d6b, idProduct=0002
[    2.919181] usb 1-1: New USB device found, idVendor=0424, idProduct=9514
[    3.349359] usb 1-1.1: New USB device found, idVendor=0424, idProduct=ec00
```

## 3. Change Pi's power supply configuration

**Tasks:**

This problem is for the Pi only. By default, the maximum current all USB peripherals can draw from the USB port is 600mA. This means that you can’t plug in a webcam and a WiFi module at the same time without suffering CPU brownouts. Change boot configuration to increase this limit to 1.2A for all four ports. 

Note: You need a beefy power supply (>= 2A) such as the ones provided in class to run after this modification.

In order to change the current, we need to add the following line to the boot configuration file ```/boot/config.txt```:

```
max_usb_current=1
```

Unfortunately, my iPhone charges at 600mA and my iPad charges at 2A. I don't have any device that has the charging threshold lies between 600mA and 1.2A. :-(

## 4. Add a program to the startup set

**Tasks:**

Add a new program to the startup set. 

* First, write a small program or script that periodically outputs some text to a file. You can start with a simple bash script with a loop that sleeps and calls date, appending the output to a log file. 
* After testing your program or script or program on the command line, configure either your VM or the Pi to run your script or program at startup. See notes for examples of this process. 
* How do you make sure it really ran? 
* What problems (if any) did you encounter?


Here is a simple script that add a line of string to a file in /tmp every five seconds.

```
mingwei@hackvm:~$ cat simple.sh
#!/bin/bash

while true; do
    echo "The date is: $(date)" >> /tmp/datetime
    sleep 5
done &
```

Here is a simple run of the script

```
mingwei@hackvm:~$ ./simple.sh
#after some seconds ^C
mingwei@hackvm:~$ cat /tmp/datetime
The date is: Tue Apr 14 00:40:16 EDT 2015
The date is: Tue Apr 14 00:44:29 EDT 2015
The date is: Tue Apr 14 00:44:34 EDT 2015
The date is: Tue Apr 14 00:44:39 EDT 2015
The date is: Tue Apr 14 00:44:44 EDT 2015
The date is: Tue Apr 14 00:44:49 EDT 2015
The date is: Tue Apr 14 00:44:54 EDT 2015
The date is: Tue Apr 14 00:44:59 EDT 2015
```

```
mingwei@hackvm:/etc/init.d$ sudo cp ~/simple.sh simple-script
```

```
mingwei@hackvm:/etc/init.d$ sudo update-rc.d simple-script defaults
update-rc.d: warning: /etc/init.d/simple-script missing LSB information
update-rc.d: see <http://wiki.debian.org/LSBInitScripts>
 Adding system startup for /etc/init.d/simple-script ...
   /etc/rc0.d/K20simple-script -> ../init.d/simple-script
   /etc/rc1.d/K20simple-script -> ../init.d/simple-script
   /etc/rc6.d/K20simple-script -> ../init.d/simple-script
   /etc/rc2.d/S20simple-script -> ../init.d/simple-script
   /etc/rc3.d/S20simple-script -> ../init.d/simple-script
   /etc/rc4.d/S20simple-script -> ../init.d/simple-script
   /etc/rc5.d/S20simple-script -> ../init.d/simple-script
```

After reboot, we can check if the script starts or not by using ```ps``` command with ```grep``` command:
```
mingwei@hackvm:~$ ps aux |grep simple
root       884  0.0  0.0  17968   920 ?        S    01:03   0:00 /bin/bash /etc/rc2.d/S20simple-script start
mingwei   1790  0.0  0.0  15940   920 pts/1    S+   01:04   0:00 grep simple
```

Then if we take a look at the ```/tmp/datetime``` file, we can see that the script keeps adding new content to the file every five seconds:

```
mingwei@hackvm:~$ cat /tmp/datetime
The date is: Tue Apr 14 01:03:07 EDT 2015
The date is: Tue Apr 14 01:03:12 EDT 2015
The date is: Tue Apr 14 01:03:18 EDT 2015
The date is: Tue Apr 14 01:03:23 EDT 2015
The date is: Tue Apr 14 01:03:28 EDT 2015
The date is: Tue Apr 14 01:03:33 EDT 2015
...
```

## 5. Extend the program to a init script

**Tasks:**

Extend the simple script you used to load your program at startup with the standard **start**, **stop**, **restart**, **status**, **reload**, and **probe options** with appropriate implementation (if any) for your program ([example][init_script] in notes). Test each of these command-line options and make sure that it does what it's supposed to.

[init_script]: https://www.linux.com/learn/tutorials/442412-managing-linux-daemons-with-init-scripts

First, we need to remove the existing rc scripts:

```
mingwei@hackvm:/etc/init.d$ sudo update-rc.d -f simple-script remove
 Removing any system startup links for /etc/init.d/simple-script ...
   /etc/rc0.d/K20simple-script
   /etc/rc1.d/K20simple-script
   /etc/rc2.d/S20simple-script
   /etc/rc3.d/S20simple-script
   /etc/rc4.d/S20simple-script
   /etc/rc5.d/S20simple-script
   /etc/rc6.d/K20simple-script
```

The extended version of the program is:

```
#!/bin/bash

start() {
    bash /home/mingwei/simple.sh &

    echo "/home/mingwei/simple.sh started"

    return
}

stop() {
    pkill simple.sh 2>&1 > /dev/null

    echo "/home/mingwei/simple.sh stopped"

    return
}

case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    status)
        ps cax | grep simple.sh > /dev/null
        if [ $? -eq 0 ]; then
          echo "simple.sh is running."
        else
          echo "simple.sh is not running."
        fi
        ;;
    restart)
        stop
        start
        ;;
    reload)
        [ -f /var/lock/subsys/ ] && restart || :
        ;;
    probe)

        ;;
    *)
        echo "Usage:  {start|stop|status|reload|restart[|probe]}"
        exit 1
        ;;
esac
exit $?
```

Before we start the service, we can try the status command to see if we can detect the service's running status:

```
mingwei@hackvm:/etc/init.d$ service simple-script status
simple.sh is not running.
```

Then we can start the service and test the status again:

```
mingwei@hackvm:/etc/init.d$ sudo service simple-script start
/home/mingwei/simple.sh started

mingwei@hackvm:/etc/init.d$ sudo service simple-script status
simple.sh is running.

mingwei@hackvm:/etc/init.d$ ps aux |grep simple.sh
root      2461  0.0  0.0  25040  1608 pts/1    S    01:35   0:00 bash /home/mingwei/simple.sh
mingwei   2475  0.0  0.0  15936   936 pts/1    S+   01:35   0:00 grep simple.sh
```

Now we can test to stop the service:

```
mingwei@hackvm:/etc/init.d$ sudo service simple-script stop
/home/mingwei/simple.sh stopped

mingwei@hackvm:/etc/init.d$ sudo service simple-script status
simple.sh is not running.
```

Sucess!

## 6. Extend the program to do something more useful

**Tasks:**

Extend your daemon program or script to do something more useful or interesting than simply output timestamps. For example, you can log interesting system statistics or look up information remotely and log that (specifics and details are up to you).

I can use the ```ngrep``` program to keep log of all the HTTP POST and GET request sent from the machine. Here is the new script that going to be called by the init script:

```
#!/bin/bash

ngrep -W byline '^POST|^GET' 'tcp and port 80' >> /tmp/httpcap
```

The I can just simply change the previous script's start and stop command to incorportate the ngrep command:

```
start() {
    #/home/mingwei/simple.sh &
    ngrep -W byline '^POST|^GET' 'tcp and port 80' -O /tmp/pcapdump 2>&1 > /dev/null &
    echo "ngrep started"
    return
}

stop() {
    pidof ngrep | xargs kill -9
    echo "ngrep stopped"
    return
}
```

After I started the service, the daemon will automatically record all the HTTP POST and GET requests, and dump the packet into the file at ```/tmp/pcapdump```.

Here is some example output of the dump:

```
interface: eth0 (10.0.2.0/255.255.255.0)
filter: (ip or ip6) and ( tcp and port 80 )
match: ^POST|^GET
########
T 10.0.2.15:43933 -> 91.189.91.23:80 [AP]
GET /ubuntu/dists/trusty/InRelease HTTP/1.1.
Host: us.archive.ubuntu.com.
Cache-Control: max-age=0.
Accept: text/*.
User-Agent: Debian APT-HTTP/1.3 (1.0.1ubuntu2).
.

#
T 10.0.2.15:52233 -> 91.189.91.13:80 [AP]
GET /ubuntu/dists/trusty-security/InRelease HTTP/1.1.
Host: security.ubuntu.com.
Cache-Control: max-age=0.
Accept: text/*.
User-Agent: Debian APT-HTTP/1.3 (1.0.1ubuntu2).
.

...
```

