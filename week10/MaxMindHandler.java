package bgpi.utils.geolocation;

import bgpi.utils.io.FileOp;
import bgpi.utils.io.Output;
import com.maxmind.geoip.Location;
import com.maxmind.geoip.LookupService;
import org.javatuples.Pair;

import java.io.IOException;

/**
 * Created by mingwei on 8/9/14.
 * APIs using MaxMind
 */
public class MaxMindHandler {
    LookupService cityl;
    LookupService countryl;
    LookupService asnl;

    public MaxMindHandler(String urlBase) {
        initData(urlBase);
    }

    public MaxMindHandler(){
        initData("http://panther.cs.uoregon.edu/GeoIP");
    }

    private void initData(String urlBase){
        String folder = "lib/";
        String asFile = "GeoIPASNum.dat";
        String ipFile = "GeoIP.dat";
        String cityFile = "GeoLiteCity.dat";
        if (!FileOp.fileExists(folder+asFile)) {
            FileOp.downloadFile(urlBase + "/GeoIPASNum.dat", folder, asFile);
            Output.pl("\tDownload GeoIPASNum.data");
        }
        if (!FileOp.fileExists(folder+ipFile)) {
            FileOp.downloadFile(urlBase + "/GeoIP.dat", folder, ipFile);
            Output.pl("\tDownload GeoIP.data");
        }
        if (!FileOp.fileExists(folder+cityFile)) {
            FileOp.downloadFile(urlBase + "/GeoLiteCity.dat", folder, cityFile);
            Output.pl("\tDownload GeoLiteCity.data");
        }

        try {
            asnl = new LookupService(folder+asFile, LookupService.GEOIP_MEMORY_CACHE);
            countryl = new LookupService(folder+ipFile, LookupService.GEOIP_MEMORY_CACHE);
            cityl = new LookupService(folder+cityFile, LookupService.GEOIP_MEMORY_CACHE);
        } catch (IOException e) {
            e.printStackTrace();
            Output.pl(urlBase);

        }
    }

    public static void main(String[] args) {
        MaxMindHandler mx = new MaxMindHandler();

        for(String prefix: args) {
            Pair<String, String> pair = mx.getPrefixCity(prefix);
            Output.pl("%s: %s %s", prefix, pair.getValue0(), pair.getValue1());
        }
    }

    public void closeAll() {
        asnl.close();
        countryl.close();
        cityl.close();
    }

    public void queryAll(String ip) {
        Output.pl(asnl.getOrg(ip));

    }

    public Pair<String, String> getPrefixCity(String prefix) {
        String ip = prefix.split("/")[0];
        try {
            Location l = cityl.getLocation(ip);
            return new Pair<>(l.countryCode, l.city);
        } catch (Exception e) {
            return new Pair<>("null", "null");
        }
    }

    public String getCountryCode(String prefix) {
        String ip = prefix.split("/")[0];
        try {
            String code = countryl.getCountry(ip).getCode();
            return code;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public Pair<Double, Double> getPrefixLocation(String prefix) {
        String ip = prefix.split("/")[0];
        try {
            Location l = cityl.getLocation(ip);
            return new Pair<>(l.latitude + 0.0, l.longitude + 0.0);
        } catch (Exception e) {
            return new Pair<>(0.0, 0.0);
        }
    }

    public Pair<Float, Float> getPrefixCountryLocation(String prefix) {
        String ip = prefix.split("/")[0];
        try {
            Location l = countryl.getLocation(ip);
            return new Pair<>(l.latitude, l.longitude);
        } catch (Exception e) {
            return new Pair<>((float) 0.0, (float) 0.0);
        }
    }

    public String getPrefixOrg(String prefix) {
        try {
            return asnl.getOrg(prefix.split("/")[0]);
        } catch (Exception e) {
            return "";
        }
    }
}
