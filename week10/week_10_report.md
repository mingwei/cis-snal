# Week 10 Report

## 1. Determining the AS and Geographic Path

*In this exercise, our goal is to find the AS and geographic path between UO to a given target IP
address. To do this, you should use traceroute from a node at UO towards the following IP addresses
to obtain the IP level path to reach the target. Then, for each IP address associated with a hop on 
your traceroute data, you should*

* *find its corresponding AS (or ISP) using team Cymru IP to AS mapping (www.teamcymru.com/).
This enables you to convert the IP level output of traceroute to AS level path that
shows which ASes your packet traverses to get to each destination. You can also identify the
inter-AS links (pair of adjacent IP addresses) that connect each pair of adjacent ASes along
the path. Report the AS path from UO to each target IPs as a collection of ASN in the order
that they are traversed (\eg ASN1, ASN2, ASN3).*
* *identify its geo location using the available IP to geography mapping services that are available
online (the free version of the service from Maxmind.com is a good choice). This partially
reliable ip to geo mapping reveals which cities your packet traverses to reach its destination.
Report this information and in particular report the cities where individual inter-AS links are
located.*

*Three suggested target IP addresses:*

* *184.50.85.91*
* *8.8.8.8*
* *69.53.236.21*


The problem could be rephrased to this: conduct traceroute the the 3 targeted IP addresses and provide AS and geolocation information of the IP addresses on the path.

For Geolocation lookup, I used the Java wrapper that I've coded before for MaxMind. 

### 184.50.85.91

**IP path:**

```
mingwei@ht-2:~$ traceroute -nA  184.50.85.91
traceroute to 184.50.85.91 (184.50.85.91), 30 hops max, 60 byte packets
 1  128.223.6.2 [AS3582]  0.383 ms  0.595 ms  0.685 ms
 2  128.223.3.9 [AS3582]  0.278 ms  0.315 ms  0.316 ms
 3  198.32.163.217 [AS4600]  2.713 ms  2.719 ms  2.703 ms
 4  198.71.47.205 [*]  6.088 ms  6.092 ms  6.076 ms
 5  64.57.20.222 [*]  28.014 ms  28.041 ms  28.024 ms
 6  64.57.20.127 [*]  27.058 ms  27.151 ms  27.143 ms
 7  * * *
 8  63.218.107.206 [AS19710/AS3491]  187.685 ms  187.735 ms  187.730 ms
 9  184.50.85.91 [AS20940]  187.714 ms  187.686 ms  187.632 ms
```

**AS path:**

AS3582 -> AS4600 -> AS19710/AS3491 -> AS20940

**Geolocations:**

```
mingwei@dyna6-71:~/Tmp$ java -jar maxmind.jar 128.223.6.2  128.223.3.9  198.32.163.217 198.71.47.205 64.57.20.222 64.57.20.127  63.218.107.206 184.50.85.91
128.223.6.2: US Eugene
128.223.3.9: US Eugene
198.32.163.217: US null
198.71.47.205: US Ann Arbor
64.57.20.222: US Ann Arbor
64.57.20.127: US Ann Arbor
63.218.107.206: US Herndon
184.50.85.91: US Cambridge
```


### 8.8.8.8

**IP path:**

```
mingwei@ht-2:~$ traceroute -nA 8.8.8.8
traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
 1  128.223.6.2 [AS3582]  2.185 ms  2.154 ms  2.172 ms
 2  128.223.3.9 [AS3582]  0.310 ms  0.345 ms  0.339 ms
 3  207.98.68.181 [AS3701]  0.283 ms  0.276 ms  0.257 ms
 4  207.98.64.162 [AS3701]  3.475 ms  3.470 ms  3.452 ms
 5  207.98.64.10 [AS3701]  3.438 ms  3.436 ms  3.479 ms
 6  * * *
 7  209.85.247.15 [AS15169]  6.494 ms 72.14.238.122 [AS15169]  6.501 ms 72.14.233.249 [AS15169]  7.269 ms
 8  8.8.8.8 [AS15169]  6.890 ms  6.278 ms  6.277 ms
```

**AS path:**

AS3582 -> AS3701 -> AS15169

**Geolocations:**

```
mingwei@dyna6-71:~/Tmp$ java -jar maxmind.jar 128.223.6.2 128.223.3.9 207.98.68.181 207.98.64.162 207.98.64.10 209.85.247.15  8.8.8.8
128.223.6.2: US Eugene
128.223.3.9: US Eugene
207.98.68.181: US Eugene
207.98.64.162: US Eugene
207.98.64.10: US Eugene
209.85.247.15: US Mountain View
8.8.8.8: US null
```

### 69.53.236.21

**IP path:**

```
mingwei@ht-2:~$ traceroute -nA 69.53.236.21
traceroute to 69.53.236.21 (69.53.236.21), 30 hops max, 60 byte packets
 1  128.223.6.2 [AS3582]  2.393 ms  2.362 ms  2.379 ms
 2  128.223.3.9 [AS3582]  0.380 ms  0.376 ms  0.357 ms
 3  207.98.68.181 [AS3701]  0.255 ms  0.285 ms  0.266 ms
 4  207.98.64.161 [AS3701]  0.692 ms  0.706 ms  0.692 ms
 5  4.53.200.1 [AS3356]  114.788 ms  114.819 ms  114.802 ms
 6  * 4.69.152.151 [AS3356]  14.450 ms 4.69.152.88 [AS3356]  14.619 ms
 7  4.71.112.78 [AS3356]  13.900 ms  22.667 ms  22.692 ms
 8  216.156.84.6 [AS2828]  14.159 ms  14.166 ms  14.167 ms
 9  69.53.225.30 [AS40027]  14.993 ms  15.107 ms  15.088 ms
10  69.53.225.10 [AS40027]  15.458 ms  15.435 ms  15.524 ms
11  69.53.236.21 [AS55095/AS2906/AS40027]  15.189 ms  15.042 ms  15.084 ms
```

**AS path:**

AS3582 -> AS3701 -> AS3356 -> AS2828 -> AS40027

**Geolocations:**

```
mingwei@dyna6-71:~/Tmp$ java -jar maxmind.jar  128.223.6.2  128.223.3.9  207.98.68.181  207.98.64.161 4.53.200.1 4.69.152.151 4.71.112.78 216.156.84.6 69.53.225.30 69.53.225.10 69.53.236.21
128.223.6.2: US Eugene
128.223.3.9: US Eugene
207.98.68.181: US Eugene
207.98.64.161: US Eugene
4.53.200.1: US null
4.69.152.151: US null
4.71.112.78: US null
216.156.84.6: US null
69.53.225.30: US Los Gatos
69.53.225.10: US Los Gatos
69.53.236.21: US San Jose
```

## 2. End-to-End Bandwidth Measurement

**iperf requires access to both end points in order to measure bandwidth/throughput between them.
Use iperf to measure bandwidth/throughput from your machine at home and your Pi at Deschutes
Hall.**

I performed the iperf3 measurement from my lab's machine in DES360 to a server machine in rack room.
Here is the result:

```
mingwei@dyna6-71:~$ iperf3 -c cougar.cs.uoregon.edu
Connecting to host cougar.cs.uoregon.edu, port 5201
[  5] local 128.223.6.71 port 63284 connected to 128.223.8.85 port 5201
[ ID] Interval           Transfer     Bandwidth
[  5]   0.00-1.00   sec  84.0 MBytes   705 Mbits/sec
[  5]   1.00-2.00   sec   106 MBytes   887 Mbits/sec
[  5]   2.00-3.00   sec   104 MBytes   876 Mbits/sec
[  5]   3.00-4.00   sec   107 MBytes   902 Mbits/sec
[  5]   4.00-5.00   sec   105 MBytes   880 Mbits/sec
[  5]   5.00-6.00   sec   107 MBytes   899 Mbits/sec
[  5]   6.00-7.00   sec   108 MBytes   906 Mbits/sec
[  5]   7.00-8.00   sec   110 MBytes   923 Mbits/sec
[  5]   8.00-9.00   sec   108 MBytes   910 Mbits/sec
[  5]   9.00-10.00  sec   110 MBytes   925 Mbits/sec
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth
[  5]   0.00-10.00  sec  1.03 GBytes   881 Mbits/sec                  sender
[  5]   0.00-10.00  sec  1.03 GBytes   881 Mbits/sec                  receiver

iperf Done.
```

## 3. Receiver Bandwidth Measurement

YouTube traffic were sent using IPv6 address, and thus will always has a IPv6 destnation address ```2001:468:d01:6:884a:8c53:ee4d:9ccf```, which is my machine's local IPv6 address.

First, we capture all the traffic information into a pcap file using `tshark`:

```
sudo tshark -i en0 "dst 2001:468:d01:6:884a:8c53:ee4d:9ccf" -w youtube.pcap
```

Then we extract the related information into CSV format:

```
sudo tshark -r youtube.pcap -T fields -e frame.time_relative -e frame.len -E header=y -E separator=,> youtube.csv
```

Then we parse the file to generate a running-average data file, using the following python script:

```
#!/usr/bin/python

import csv
import sys
import collections

f = open(sys.argv[1], 'rt')

avg = collections.deque(maxlen=10)

try:
    reader = csv.reader(f)
    cur_sec = 0
    cur_sum = 0
    for r in reader:
        sec = float(r[0])
        size = int(r[1])
        if sec < cur_sec+1:
            cur_sum += size
        else:
            avg.append(cur_sum)
            print cur_sec, ',', sum(avg)/len(avg)
            cur_sum = 0
            cur_sec += 1
            while sec > cur_sec+1:
                avg.append(0)
                print cur_sec, ',', sum(avg)/len(avg)
                cur_sec+=1
finally:
    f.close()
```

Here is the plotted results:

![](bandwidth_smoothed.png)

## 4. Multiple Gateway Problem

I plugged in my Pi into ethernet network, and also connected it into WiFi (UOSecure).

I have no problem accessing it from home (Spencer View network) or from department's UOSecure access point.

## 5. ARP Poisoning

Basic setup:

* Raspberry Pi (**Attacker**): 192.168.0.101
* Mac Air (**Victim**): 192.168.0.102
* Linux Box (**HTTP server**): 192.168.0.103

On the Raspberry Pi node, we run the software [ettercap](https://ettercap.github.io/ettercap/).
It could be run in a GUI mode, like figure below:

![ettercap1.png](ettercap1.jpg)

We then specify Pi (**192.168.0.101**) to spoof the Linux box (**192.168.0.103**).
As we shown in the figure below, the ARP cache on the Mac Air shows that both 101 and 103 has the same MAC address, which is belonged to the Raspberry Pi.

![](arpspoof.png)

Success!

In order to detect such spoofing, we need to do it on the victim machine.
The victim should run a ```tcpdump -i ethX arp``` to keep track of all the ARP replies it heard
on the segment.
If any of the replies contains the victim's IP address with a different MAC address, 
then the victim can safely say that there is someone trying to conduct ARP spoofing.