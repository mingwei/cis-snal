#!/usr/bin/python

import csv
import sys
import collections

f = open(sys.argv[1], 'rt')

avg = collections.deque(maxlen=10)

try:
    reader = csv.reader(f)
    cur_sec = 0
    cur_sum = 0
    for r in reader:
        sec = float(r[0])
        size = int(r[1])
        if sec < cur_sec+1:
            cur_sum += size
        else:
            avg.append(cur_sum)
            print cur_sec, ',', sum(avg)/len(avg)
            cur_sum = 0
            cur_sec += 1
            while sec > cur_sec+1:
                avg.append(0)
                print cur_sec, ',', sum(avg)/len(avg)
                cur_sec+=1
finally:
    f.close()
