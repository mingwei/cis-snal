#!/bin/bash

onemonth=`date --date="1 month ago" +"%y%m%d"`
last -F -R| head -n -2| cut -c1-9,27-46 | grep -v "reboot"| while read line; do date=`date -d "$(echo $line | awk '{ print $2" "$3" "$4" "$5 }')" +"%y%m%d"`; [[ $date -ge "$onemonth" ]] && echo $line; done > output.txt
