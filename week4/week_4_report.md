# week 4 report

## 1.

Create user accounts on your pi for everybody in the class. Collect and share a list of usernames during class. This means that your Pi should be accessible as much as possible during the week (share IP addresses or other specific info required for ssh-ing into the Pi you "own" with your "users"). Do NOT email passwords.

Here is the link that contains everyone's preffered username and public keys: [https://docs.google.com/spreadsheets/d/1qmY3vcf68yb9L2VM9TPjpUtoQlgRoXHiS0Jhvv0oIWw/edit?usp=sharing](https://docs.google.com/spreadsheets/d/1qmY3vcf68yb9L2VM9TPjpUtoQlgRoXHiS0Jhvv0oIWw/edit?usp=sharing)

Here is the script to create users based on the username and ssh public key they provided:

``` bash
#!/bin/bash

while IFS='=' read USER KEY
do
    if [ ! -d "/home/$USER" ]; then
        sudo useradd -m $USER -g snal
        echo created user $USER
        sudo su - $USER -c "mkdir /home/$USER/.ssh; echo $KEY > /home/$USER/.ssh/authorized_keys; chmod 600 /home/$USER/.ssh/authorized_keys"
    else
        echo $USER already created
    fi
done < credentials.txt
```

The command to connect the server behind the ```brix```.

```
ssh -A -t snail@brix.d.cs.uoregon.edu ssh -A -p2224 mingwei@localhost
```

After we created two accounts for us (Lumin and I), we gave these two accounts sudo permission by editing the ```/etc/sudoer```. 
After that, we disabled the default pi account to prevent any logged on users to switch to this account and do potentially bad things.

```
sudo passwd pi -l
```
	
## 2.

Write a script to check who has logged in into your Pi in the past month and log this information (usernames and last login) to a file.

Lumin and I collaboratively crafted a script file that can accomplish the task:

```
#!/bin/bash
 
onemonth=`date --date="1 month ago" +"%y%m%d"`
last -F -R| head -n -2| cut -c1-9,27-46 | grep -v "reboot"|\
while read line;\
do date=`date -d "$(echo $line | awk '{ print $2" "$3" "$4" "$5 }')" +"%y%m%d"`; [[ $date -ge "$onemonth" ]] && echo $line;\
done > output.txt
```

Here let me explain what each part does.

First we get the starting date, which is one month ago:

```
onemonth=`date --date="1 month ago" +"%y%m%d"`
```

Then we do the following things to get the formatted login information

* print out all the login information
	*  ```last -F -R```
* remove the last two useless lines
	*  ```head -n -2```
* only show the username and the login time
	*  ```cut -c1-9,27-46```
	*  here we manually counted the positions for the user and login time output.
* remove the lines that triggered by reboot action
	*  ```grep -v "reboot"```

Then we pipe the output to a while loop like following that compare the dates of logins with the time of one month ago. If it is greater than that, then we will print the line out to a output file.

```
 while read line
 do 
 	date=`date -d "$(echo $line | awk '{ print $2" "$3" "$4" "$5 }')" +"%y%m%d"`
 	[[ $date -ge "$onemonth" ]] && echo $line
 done > output.txt
```

The output of our script on the pi likes like this:

```
mingwei Apr 22 22:32:41 2015
mingwei Apr 22 21:33:38 2015
lshi Apr 22 21:16:32 2015
lshi Apr 22 21:12:25 2015
pi Apr 22 21:12:15 2015
mingwei Apr 22 21:11:50 2015
pi Apr 22 18:17:23 2015
pi Apr 22 18:17:15 2015
npotter Apr 21 22:40:28 2015
mingwei Apr 21 20:32:27 2015
pi Apr 21 20:32:19 2015
pi Apr 21 20:24:13 2015
pi Apr 21 20:22:35 2015
pi Apr 21 20:18:50 2015
npotter Apr 20 22:00:27 2015
npotter Apr 20 20:23:22 2015
npotter Apr 20 18:53:52 2015
npotter Apr 20 18:49:35 2015
pi Apr 20 16:33:34 2015
pi Apr 20 16:06:09 2015
...
```

## 3.

Write a short bash script that does some logging of system state. Feel free to vary what the script monitors. For example, you can check system load and number of users at regular intervals and log this information in a file (other options are disk usage, e.g., to detect when nearly full, too many failed login attempts, and network connectivity). You can make your script a daemon, or have it be a cron job. Describe what you intended to do in your report and of course add the script to the repo.

Log the number of processes the system is running every 5 minutes:

```
#!/bin/bash

echo `date` `ps aux|wc -l` >> output.txt
```

I added this to crontab by ```crontab -e```:

```
*/5 * * * * bash /home/mingwei/log_processes.sh >/dev/null 2>&1
```

## 4.

Write a shell script that takes as input a file size (in bytes) and creates a text file containing all the files in the system that are larger than that size, sorted by size. Each line should contain the full path name of the file, followed by a tab, followed by the size in bytes.

See the script below:

```
#!/bin/bash

if [ $# -ne 1 ]
then
    echo "USAGE: bash larger_files.sh SIZE"; exit 1
fi

re='^[0-9]+$'
if ! [[ $1 =~ $re ]] ; then
   echo "ERROR: Not a number" >&2; exit 1
fi

SIZE=$1

find / -type f -size +${SIZE}c -exec ls -ln {} \;| awk '{ print $9 "\t" $5 }' > output.txt
```

## 5.

Graduate students only (optional for undergrads): write a script (either daemon or cron job) to check for intrusion similar to what happened to the Pis that got hacked last week.

Our Pi has not been hacked due to our restriction on password login. 
I don't know how to detect intrusions and it seems to be a rather hard problem to solve using bash script. 