#!/bin/bash

if [ $# -ne 1 ]
then
    echo "USAGE: bash larger_files.sh SIZE"; exit 1
fi

re='^[0-9]+$'
if ! [[ $1 =~ $re ]] ; then
   echo "ERROR: Not a number" >&2; exit 1
fi

SIZE=$1

find / -type f -size +${SIZE}c -exec ls -ln {} \;| awk '{ print $9 "\t" $5 }'
