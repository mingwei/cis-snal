# Week 6 Report

# 1. package management

*What packages are installed on the linux machines you have access to (ix, the Pi, your VM)? In your report, just describe how you found out and how many packages you saw (don't include a full list!)*

Since all the machines I have access to are Debian based, I can use ```dpkg -l``` to list all the packages installed on these machines. 
To count the number of packages, I pipe result into ```wc -l``` and count how many lines are there.

* **Pi**: 995 
* **ix**: 1510
* **VM**: 1967


# 2. Google Chrome from commandline

*Install the Google Chrome browser on your VM. Include the commands you used in your report.*

First, it is obvious that we can download chrome package from their website to install it.

To install Google Chrome from commandline, we can follow the instruction [here](http://askubuntu.com/questions/79280/how-to-install-chrome-browser-properly-via-command-line):

```
sudo apt-get install libxss1 libappindicator1 libindicator7
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome*.deb
```

```
mingwei@hackvm:~$ sudo dpkg -i google-chrome-stable_current_amd64.deb
Selecting previously unselected package google-chrome-stable.
(Reading database ... 215200 files and directories currently installed.)
Preparing to unpack google-chrome-stable_current_amd64.deb ...
Unpacking google-chrome-stable (42.0.2311.135-1) ...
Setting up google-chrome-stable (42.0.2311.135-1) ...
update-alternatives: using /usr/bin/google-chrome-stable to provide /usr/bin/x-www-browser (x-www-browser) in auto mode
update-alternatives: using /usr/bin/google-chrome-stable to provide /usr/bin/gnome-www-browser (gnome-www-browser) in auto mode
update-alternatives: using /usr/bin/google-chrome-stable to provide /usr/bin/google-chrome (google-chrome) in auto mode
Processing triggers for man-db (2.6.7.1-1ubuntu1) ...
Processing triggers for mime-support (3.54ubuntu1.1) ...
Processing triggers for gnome-menus (3.10.1-0ubuntu2) ...
Processing triggers for desktop-file-utils (0.22-1ubuntu1) ...
Processing triggers for bamfdaemon (0.5.1+14.04.20140409-0ubuntu1) ...
Rebuilding /usr/share/applications/bamf-2.index...
```

# 3. install other packages

*Install some new useful (to you) packages. Some ideas -- editors, different version of the compiler, a game, browser, etc. Describe how you found and installed them. Next, create your own package on your Ubuntu VM, following these [instructions](http://packaging.ubuntu.com/html/packaging-new-software.html). Feel free to package your own program instead of the provided hello-2.7.tar.gz.*	

For all the Linux boxes, I will always install the following softwares:

* **ncdu**: a ncurses based disk utility software, super useful and intuitive.
* **vim**: many Linux boxes do not come with Vim but just Vi
* **nethack**: *if you only play one game in your life, play nethack*

The software can be directly installed through ```apt-get```:

```
mingwei@hackvm:~$ sudo apt-get install ncdu nethack-console vim -y
```

**packaging**

I first modified the hello world program a little bit:

```
mingwei@hackvm:~/hello-2.7$ hello
Hello, world from mingwei!
```

```
mingwei@hackvm:~$ bzr dh-make hello-mingwei 2.7 hello-mingwei-2.7.tar.gz
Fetching tarball
Looking for a way to retrieve the upstream tarball
Upstream tarball already exists in build directory, using that

Type of package: single binary, indep binary, multiple binary, library, kernel module, kernel patch?
 [s/i/m/l/k/n] s

Maintainer name  : Mingwei Zhang
Email-Address    : mingwei@unknown
Date             : Tue, 05 May 2015 01:09:06 -0400
Package Name     : hello-mingwei
Version          : 2.7
License          : blank
Type of Package  : Single
Hit <enter> to confirm:
Skipping creating ../hello-mingwei_2.7.orig.tar.gz because it already exists
Done. Please edit the files in the debian/ subdirectory now. hello-mingwei
uses a configure script, so you probably don't have to edit the Makefiles.
Package prepared in /home/mingwei/hello-mingwei
```

In order to build with the changes in the source file, we need to modify the tests as well, including ```hello-1``` and ```traditional-1```.

It just kept promoting some detailed errors, so I didn't get all the way through the end.

```
E: hello changes: bad-distribution-in-changes-file unstable
W: hello source: package-needs-versioned-debhelper-build-depends 9
E: hello source: maintainer-address-malformed Mingwei Zhang <mingwei@unknown>
W: hello source: superfluous-clutter-in-homepage <insert the upstream URL, if relevant>
W: hello source: bad-homepage <insert the upstream URL, if relevant>
W: hello source: out-of-date-standards-version 3.9.4 (current is 3.9.5)
W: hello: wrong-name-for-upstream-changelog usr/share/doc/hello/ChangeLog.O.gz
W: hello: wrong-bug-number-in-closes l3:#nnnn
W: hello: new-package-should-close-itp-bug
E: hello: changelog-is-dh_make-template
E: hello: helper-templates-in-copyright
W: hello: copyright-has-url-from-dh_make-boilerplate
E: hello: copyright-contains-dh_make-todo-boilerplate
W: hello: readme-debian-contains-debmake-template
E: hello: description-is-dh_make-template
E: hello: maintainer-address-malformed Mingwei Zhang <mingwei@unknown>
E: hello: section-is-dh_make-template
W: hello: superfluous-clutter-in-homepage <insert the upstream URL, if relevant>
W: hello: bad-homepage <insert the upstream URL, if relevant>
E: hello: package-contains-info-dir-file usr/share/info/dir.gz
```

# 4. list kernel modules on Pi and VM

*What kernel modules are loaded on your Pi and VM? Pick one and tell me what it is for (in your report). Next unload one of the sound modules and reload it (show commands and output).*

We can use ```less /proc/modules``` or ```lsmod``` to list all the loaded modules on any Linux box. We can also use ```modinfo```.

On **VM**: 
usbhid: USB HID core driver

On **VM**:
snd: Advanced Linux Sound Architecture driver for soundcards.

I tried to directly unload the ```snd``` module on Pi, and it failed:

```
mingwei@duckpi02 ~ $ sudo modprobe -r snd
FATAL: Module snd is in use.
```

It turned out that ```snd``` module is used by 5 other moduels:

```
mingwei@duckpi02 ~ $ lsmod
Module                  Size  Used by
snd_bcm2835            18850  0
snd_pcm                75388  1 snd_bcm2835
snd_seq                53078  0
snd_seq_device          5628  1 snd_seq
snd_timer              17784  2 snd_pcm,snd_seq
snd                    51667  5 snd_bcm2835,snd_timer,snd_pcm,snd_seq,snd_seq_device
md_mod                108885  0
spi_bcm2708             5153  0
uio_pdrv_genirq         2958  0
uio                     8119  1 uio_pdrv_genirq
```

So, in order to success in reload a module, I chose to work on a not used module ```snd_seq```, which is a "Advanced Linux Sound Architecture sequencer."

**removal**

```
mingwei@duckpi02 ~ $ sudo modprobe -r snd_seq
mingwei@duckpi02 ~ $ lsmod
Module                  Size  Used by
snd_bcm2835            18850  0
snd_pcm                75388  1 snd_bcm2835
snd_timer              17784  1 snd_pcm
snd                    51667  3 snd_bcm2835,snd_timer,snd_pcm
md_mod                108885  0
spi_bcm2708             5153  0
uio_pdrv_genirq         2958  0
uio                     8119  1 uio_pdrv_genirq
```
We can see that ```snd_seq``` is no longer in the loaded modules list.

**install**

```
mingwei@duckpi02 ~ $ sudo modprobe snd_seq
mingwei@duckpi02 ~ $ lsmod
Module                  Size  Used by
snd_seq                53078  0
snd_seq_device          5628  1 snd_seq
snd_bcm2835            18850  0
snd_pcm                75388  1 snd_bcm2835
snd_timer              17784  2 snd_pcm,snd_seq
snd                    51667  5 snd_bcm2835,snd_timer,snd_pcm,snd_seq,snd_seq_device
md_mod                108885  0
spi_bcm2708             5153  0
uio_pdrv_genirq         2958  0
uio                     8119  1 uio_pdrv_genirq
```
We can see that ```snd_seq``` is back in the loaded modules list.


# 5. modify kernel modules to match kernel version

*Graduate students only (optional for undergrads): Follow the directions [here](http://sandsoftwaresound.net/raspberry-pi/raspberry-pi-gen-1/build-a-kernel-module/), appropriately modifying them to match your kernel version. If you are building a module on your virtual machine, make sure to use the appropriate kernel and module versions. An example "hello world" module is described [here](https://github.com/notro/rpi-source/wiki/Examples-on-how-to-build-various-modules#hello-world-example).*

I did this problem in my VM, following the hello world instruction.

To start, I created the Makefile and the hello.c file, and then compiled it in order to generate the kernel module binary file.

```
mingwei@hackvm:~/hello$ make -C /lib/modules/$(uname -r)/build M=$(pwd) modules
make: Entering directory `/home/mingwei/linux-4.0.1'
  CC [M]  /home/mingwei/hello/hello.o
  Building modules, stage 2.
  MODPOST 1 modules
  CC      /home/mingwei/hello/hello.mod.o
  LD [M]  /home/mingwei/hello/hello.ko
make: Leaving directory `/home/mingwei/linux-4.0.1'
mingwei@hackvm:~/hello$ ls
hello.c   hello.mod.c  hello.o	 modules.order
hello.ko  hello.mod.o  Makefile  Module.symvers
```

As we can see, here we have ```hello.ko``` file generated. Now, we can test our modified hello world kernel to see if it can be loaded to the kernel.

```
mingwei@hackvm:~/hello$ sudo insmod hello.ko
mingwei@hackvm:~/hello$ dmesg |tail -1
[  542.170574] Hello World! From Mingwei for SNAL.
mingwei@hackvm:~/hello$ sudo rmmod hello
mingwei@hackvm:~/hello$ dmesg |tail -1
[  560.612384] Goodbye World! From Mingwei for SNAL
mingwei@hackvm:~/hello$
```

Note that, the kernel and module versions were taken care of by the make's command line option ```$(uname -r)```.


# 5a. (optional) create and use an LED driver for Pi

*(Optional for everyone): Create and use an LED driver for your Pi, following these [instructions](http://sysprogs.com/VisualKernel/tutorials/raspberry/leddriver/). I will provide the lights and wires.*

# 6. compile and install a new kernel

*(Optional for everyone, but +10 extra credit if you do this). First, check and report your current kernel version. Next, choose either your VM or the Pi and compile a new kernel from source code (I am not asking you to actually change the kernel, but if you are feeling adventurous and know what to do, please go ahead!). Test it by booting your VM or Pi. Show the output of uname -a upon a successful reboot. It is a good idea to back up your VM or Pi before doing this.*

Current VM kernel version:

```
Linux hackvm 3.13.0-49-generic #83-Ubuntu SMP Fri Apr 10 20:11:33 UTC 2015 x86_64 x86_64 x86_64 GNU/Linux
```

I will install a newest 4.0.1 Linux kernel on it, following this intruction:
[KernelBuild](http://kernelnewbies.org/KernelBuild)

After downloading, it took about 10 minutes to finish the compilation, with 4 threads concurrent processing enabled:

```
mingwei@hackvm:~/linux-4.0.1$ make defconfig; stime make -j4
...
...
Setup is 15532 bytes (padded to 15872 bytes).
System is 5821 kB
CRC e696df37
Kernel: arch/x86/boot/bzImage is ready  (#1)

real    3m12.688s
user    9m49.065s
sys     2m8.068s
```

Then we can install the new kernel to the system:

```
sudo make modules_install install
```

Next, we will update the GRUB to actually be able to use the new kernel:

```
mingwei@hackvm:~/linux-4.0.1$ sudo update-grub2
Generating grub configuration file ...
Warning: Setting GRUB_TIMEOUT to a non-zero value when GRUB_HIDDEN_TIMEOUT is set is no longer supported.
Found linux image: /boot/vmlinuz-4.0.1
Found initrd image: /boot/initrd.img-4.0.1
Found linux image: /boot/vmlinuz-3.13.0-49-generic
Found initrd image: /boot/initrd.img-3.13.0-49-generic
Found linux image: /boot/vmlinuz-3.13.0-32-generic
Found initrd image: /boot/initrd.img-3.13.0-32-generic
Found memtest86+ image: /boot/memtest86+.elf
Found memtest86+ image: /boot/memtest86+.bin
  No volume groups found
done
```

After a restart, we can check the running kernel to see if the new kernel has been used:

```
mingwei@hackvm:~$ uname -a
Linux hackvm 4.0.1 #1 SMP Mon May 4 18:30:16 EDT 2015 x86_64 x86_64 x86_64 GNU/Linux
```

We are now running a Linux 4.0.1 Kernel. Success!
