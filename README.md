# README #

CIS 410/510 System and Network Administration Labs

### Instructor ###

* Boyana Norris
* Reza Rejaie

### Important links ###

* [tasks][tasks]
* [class webpage][classpage]
* [piazza][piazza]

### Current Progress ###

#### Week 1 ####

[Exercises](https://ix.cs.uoregon.edu/~norris/cis410snal/index.cgi?n=Main.W1)
[Notes](https://ix.cs.uoregon.edu/~norris/cis410snal/notes/intro-s15.pdf)

#### Week 2 ####

[Exercises](https://ix.cs.uoregon.edu/~norris/cis410snal/index.cgi?n=Main.W2)
[Notes](https://ix.cs.uoregon.edu/~norris/cis410snal/index.cgi?n=Main.W2Notes)

#### Week 3 ####

[Exercises](https://ix.cs.uoregon.edu/~norris/cis410snal/index.cgi?n=Main.W3)
[Notes](https://ix.cs.uoregon.edu/~norris/cis410snal/index.cgi?n=Main.W3Notes)

#### Week 4 ####
[Exercises](https://ix.cs.uoregon.edu/~norris/cis410snal/index.cgi?n=Main.W4)
[Notes](https://ix.cs.uoregon.edu/~norris/cis410snal/index.cgi?n=Main.W4Notes)

#### Week 5 ####
[Exercises](https://ix.cs.uoregon.edu/~norris/cis410snal/index.cgi?n=Main.W5)

#### Week 6 ####

#### Week 7 ####

#### Week 8 ####

#### Week 9 ####

#### Week 10 ####

Project presentations and reports.


[classpage]: https://www.cs.uoregon.edu/Classes/15S/cis410snal/
[tasks]: https://ix.cs.uoregon.edu/~norris/cis410snal/index.cgi 
[piazza]: https://piazza.com/class/i7w3wo0nyv9un?cid=6
[week1]: https://ix.cs.uoregon.edu/~norris/cis410snal/index.cgi?n=Main.W1
