# Week 7 Report

## Excercises

### 1. Configuring network interfaces

#### ip command:

_In this section, you should use both the ifconfig and ip command to assign static IP address to an interface on your Pi. ip is anew interface configuration command that replace ifconfig. Learning the format of the ifconfig command is necessary since the ip command is not available on all systems. Using these two command, perform the following exercises:_
* *Explicit Assignment: Assign a static IP address to an interface of your Pi. Note this would run into conflicts with the DHCP client that is running on their machine since it would periodically requests an IP address from the DHCP server and assigns it to each interface of your computer. Therefore, you should disable the DHCP client on your computer. Boot up Assignment: Another way to assign static IP address to an interface is during the boot time of your Pi through the /etc/ network/interfaces file.** *Another piece of information that a node needs to be able to operate, is the address of a (local) DNS server. This information is usually specified in the ```/etc/resolve.conf``` file.** *You can explore the above issues on your own to have more fun or refer to the description at the following URL <https://help.ubuntu.com/lts/serverguide/network-configuration.html>** *One way to to check whether the assigned IP address is correct, is to ping it from another node on the same subnet.*

I followed this [intruction](http://www.tecmint.com/ip-command-examples/) to learn the ```ip``` command.

```
vagrant@precise32:~$ sudo ip addr add 192.168.0.201 dev eth1

vagrant@precise32:~$ ip addr show
...
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP qlen 1000
    link/ether 08:00:27:0a:8c:1c brd ff:ff:ff:ff:ff:ff
    inet 192.168.0.101/24 brd 192.168.0.255 scope global eth1
    inet 192.168.0.201/32 scope global eth1
    inet6 fe80::a00:27ff:fe0a:8c1c/64 scope link
       valid_lft forever preferred_lft forever
```

Now we have two addresses. 
From another virtual machine, we can ping this new address (192.168.0.201) to see if it works:

```
vagrant@precise32:~$ ping 192.168.0.201 -c 1
PING 192.168.0.201 (192.168.0.201) 56(84) bytes of data.
64 bytes from 192.168.0.201: icmp_req=1 ttl=64 time=0.324 ms

--- 192.168.0.201 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.324/0.324/0.324/0.000 ms
```

Success! Of course, we can use ```ip addr del``` to remove the other addresses.

#### ifconfig command:

I followed this [intruction](http://www.tecmint.com/ifconfig-command-examples/) to learn the ```ifconfig``` command usages.

```
vagrant@precise32:~$ sudo ifconfig eth1 192.168.0.203
vagrant@precise32:~$ ifconfig
...
eth1      Link encap:Ethernet  HWaddr 08:00:27:0a:8c:1c
          inet addr:192.168.0.201  Bcast:0.0.0.0  Mask:255.255.255.255
          inet6 addr: fe80::a00:27ff:fe0a:8c1c/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:270 errors:0 dropped:0 overruns:0 frame:0
          TX packets:12 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:59244 (59.2 KB)  TX bytes:956 (956.0 B)
...
```

Then we can again ping this new address (192.168.0.203) from another machine:

```
vagrant@precise32:~$ ping 192.168.0.203 -c 1
PING 192.168.0.203 (192.168.0.203) 56(84) bytes of data.
64 bytes from 192.168.0.203: icmp_req=1 ttl=64 time=0.308 ms

--- 192.168.0.203 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.308/0.308/0.308/0.000 ms
```

Success!

### 2. Managing routing table of local computer

*A computer can forward packets between its interfaces similar to a router. To do this, it requires a forwarding/routing table that shows how individual packets should be forwarded. Similar to the previous case, routing table entries may get modified by a DHCP client that is running on your node. Make sure to disable the DHCP client. The configuration for the routing entries of the computer could be done through two set of commands, namely route and ip. Using these commands practice to define static routing table entry and default route entry for the wireless interface, and manage routing entries for multiple interfaces (wireless and ethernet). To test the static or default route, you can serf them to the wireless interface and check that you can browse the Internet but you should not be able to ping other Pi's interfaces on the Ethernet subnet. To test the multi-interface setup, the connections associated with each interface should be properly routed through them to her corresponding subnet (wireless or Ethernet).*


I followed this [instruction](http://www.tecmint.com/setup-linux-as-router/) to learn about the commands for setting up the routing table.

The default routing table for my VM looks like this:

```
vagrant@precise32:~$ route -n
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         10.0.2.2        0.0.0.0         UG    0      0        0 eth0
0.0.0.0         10.0.2.2        0.0.0.0         UG    100    0        0 eth0
10.0.2.0        0.0.0.0         255.255.255.0   U     0      0        0 eth0
192.168.0.0     0.0.0.0         255.255.255.0   U     0      0        0 eth1
```

In this config, the default gateway is set to be the NAT manager of the virtual machine manager.

First, let's remove the default routes so that the ping to another should not be available.

```
vagrant@precise32:~$ ping  192.168.0.203 -c 1
PING 192.168.0.203 (192.168.0.203) 56(84) bytes of data.
64 bytes from 192.168.0.203: icmp_req=1 ttl=64 time=0.317 ms

--- 192.168.0.203 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.317/0.317/0.317/0.000 ms

vagrant@precise32:~$ sudo ip route del default
vagrant@precise32:~$ sudo ip route del default
vagrant@precise32:~$ sudo ip route del 192.168.0.0/24

vagrant@precise32:~$ ping 192.168.0.203
connect: Network is unreachable
```

After we add back the default route, the ping went back to work:

```
vagrant@precise32:~$ sudo ip route add default via 10.0.2.2

vagrant@precise32:~$ ping 192.168.0.203 -c 1
PING 192.168.0.203 (192.168.0.203) 56(84) bytes of data.
64 bytes from 192.168.0.203: icmp_req=1 ttl=63 time=0.311 ms

--- 192.168.0.203 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.311/0.311/0.311/0.000 m
```


### 3. Firewall configuration NAT/IPTables

*Firewalls are used to control/manage the connections that are established to a network (e.g. home network). For firewall configuration you should become familiar with the iptables command. Note that there is a simplified rapper aroundthe iptables command named ufw that you need to install but it doesn't give you the full expressiveness/flexibility ofof iptables. iptables has five tables, namely raw, filter, nat, mangle and security, and each table has a set of chains. Possible chains are INPUT, OUTPUT, PREROUTING, POSTROUTING and FORWARD where each chain includes a set of rules. These chains are traversed in a predefined manner, and when a packet matches a rule in one chain, the packet my not be compared (depending on the action) against the rules in other tables (i.e. the final decision for handling the packet is made). Each chain as a default action. The flow chart of a packet processing that shows all the tables and chains in a firewall is presented in the following figure: <http://rlworkman.net/howtos/iptables/images/tables_traverse.jpg>*
*Each rule could have a target that defines what action should be taken if a packet matches the defined rule. The complete list of actions (or targets) is fairly large and include __DROP, ACCEPT, RETURN, LOG__. The first two actions are clear, __RETURN__ causes a packet to be passed to the previous calling chain. __LOG__ causes a packet to be logged to the log file of iptables.Check online resources for other actions.**Using iptables command perform the following exercises:*

1. *proper firewall configuration by defining default blacklist rules for input packet,* 
2. *whitelisting benign traffic: depending on the services that you are running on you node.*
3. *logging firewall events: define a set of rules for packets that should be logged and generate the packets that satisfy those rules and are logged.*
*To experiment with these capabilities, you can block traffic based on source IP addresses (incoming flows from another Pi)or white lists incoming flows from certain sources that are destined to those services (based on their destination IP and port) on the local node.*

Here, I plan to install the following rules:

* blacklist default traffic
* whitelist all **SSH** traffic
* log all **DNS** packets

I followed these [instructions](http://www.tecmint.com/configure-iptables-firewall/).

By default, there is not entries in my VM's iptables:

```
vagrant@precise32:~$ sudo iptables -L -n -v
Chain INPUT (policy ACCEPT 0 packets, 0 bytes)
 pkts bytes target     prot opt in     out     source               destination

Chain FORWARD (policy ACCEPT 0 packets, 0 bytes)
 pkts bytes target     prot opt in     out     source               destination

Chain OUTPUT (policy ACCEPT 0 packets, 0 bytes)
 pkts bytes target     prot opt in     out     source               destination
```

To prevent us blocking ourselves out, we will first add a rule to allow all SSH traffic.

```
vagrant@precise32:~$ sudo  iptables -A INPUT -i eth0 -s 0/0 -p tcp --dport 22 -j ACCEPT

vagrant@precise32:~$ sudo iptables -L -n -v
Chain INPUT (policy ACCEPT 3 packets, 228 bytes)
 pkts bytes target     prot opt in     out     source               destination
   28  1504 ACCEPT     tcp  --  eth0   *       0.0.0.0/0            0.0.0.0/0            tcp dpt:22
``` 

Then reject all other traffic:

```
vagrant@precise32:~$ sudo iptables -P INPUT DROP

vagrant@precise32:~$ sudo iptables -L -n -v
Chain INPUT (policy DROP 0 packets, 0 bytes)
 pkts bytes target     prot opt in     out     source               destination
  116  6144 ACCEPT     tcp  --  eth0   *       0.0.0.0/0            0.0.0.0/0            tcp dpt:22
```

And now the other machines cannot ping this one any more:

```
vagrant@precise32:~$ ping 192.168.0.100 -c 1
PING 192.169.0.100 (192.168.0.100) 56(84) bytes of data.

--- 192.168.0.100 ping statistics ---
1 packets transmitted, 0 received, 100% packet loss, time 0ms
```

If we added one whitelist rule for icmp, then we can have other machines to ping this one:

```
sudo iptables -A INPUT --protocol icmp --in-interface eth1 -j ACCEPT

vagrant@precise32:~$ ping 192.168.0.100 -c 1
PING 192.168.0.100 (192.168.0.100) 56(84) bytes of data.
64 bytes from 192.168.0.100: icmp_req=1 ttl=64 time=0.051 ms

--- 192.168.0.100 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.051/0.051/0.051/0.000 ms
```

Then we use the following lines to create a logging chain and log all the dropped packets (followed this [intruction](http://www.thegeekstuff.com/2012/08/iptables-log-packets/)):

```
iptables -N LOGGING
iptables -A INPUT -j LOGGING
iptables -A LOGGING -m limit --limit 2/min -j LOG --log-prefix "IPTables-Dropped: " --log-level 4
iptables -A LOGGING -j DROP
```

It will create a new chain called "LOGGING" and log all the packets that does not belong to any of the whitelist items in the INPUT chain, and then drop them. Here is the example logged message from ```/var/log/syslog```: (note, I generated some HTTP request using ```curl``` command from another VM)

```
May 12 03:37:28 precise32 kernel: [  483.842267] IPTables-Dropped: IN=eth1 OUT= MAC=ff:ff:ff:ff:ff:ff:0a:00:27:00:00:00:08:00 SRC=192.168.0.1 DST=192.168.0.255 LEN=208 TOS=0x00 PREC=0x00 TTL=64 ID=54580 PROTO=UDP SPT=17500 DPT=17500 LEN=188
```

Success!

----

## Project

### Part 1: Configure Your Pi Into an Internet Gateway

*For your project, you should configure their Pi to act as an Internet gateway. Given a Pi, you should configure its ethernet interface as a shared interface for other nodes on the ethernet subnet to connect the Internet. This Pi that acts as a gateway, should route all the incoming flows that are destined to the Internet through the wireless interface to their destination. Your gateway node should also implement a NATing and replace the source address of the packet as it traverses the gateway. For the Internet sharing scenario, you have to enable IP forwarding for the Linux kernel which could be accomplished through various means such asthe __sysctl__ command or the __/proc/sys/net/ipv4/ip_forward__ file.*
*To run this experiment, you need to pair your Pis or have two nodes on the same ethernet subnet where one node acts as the client and the other one act as the gateway. You can assign static IP address to the ethernet interface (so there is not need to run a DHCP server).*

The machines setup:

* gateway: 
	* __eth1__: 192.168.0.100
	* __eth0__: 10.0.2.15 (connected to the Internet)
* host:
	* __eth1__: 192.168.0.201


I mostly followed the steps descriped [here](http://www.tecmint.com/setup-linux-as-router/) for this problem.

#### host settings

First, I will move all the default routes from the host VM, and now it does not know the route to anywhere:

```
vagrant@precise32:~$ sudo ip route del default
vagrant@precise32:~$ ping google.com
connect: Network is unreachable
```

Then add the gateway VM as the default gateway:

```
vagrant@precise32:~$ sudo ip route add default via 192.168.0.100
vagrant@precise32:~$ route
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
default         192.168.0.100   0.0.0.0         UG    0      0        0 eth1
10.0.2.0        *               255.255.255.0   U     0      0        0 eth0
192.168.0.0     *               255.255.255.0   U     0      0        0 eth1
```

#### gateway settings

First, we need to enable the traffic forwarding from system level:

```
# echo 1 > /proc/sys/net/ipv4/ip_forward
```

Then we add the packet forwarding rules here:

```
vagrant@precise32:~$ sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE

vagrant@precise32:~$ sudo iptables -A FORWARD -i eth0 -o eth1 -m state --state RELATED,ESTABLISHED -j ACCEPT

vagrant@precise32:~$ sudo iptables -A FORWARD -i eth1 -o eth0 -j ACCEPT
```

First, we add a rule to the POSTROUTING chian inthe nat table, indicating that the ```eth0``` interface should be used as our outgoing nat interface. The option MASQERADE indicates that we need to translate the source address to a public address before we forward the traffic out. Otherwise, the return packet will never get back to us.

Then we add the forwarding rules to accept the forwarding traffic.

#### test the gateway

We can just simply ping google.com, to see if now it works or not:

```
vagrant@precise32:~$ ping google.com -c 1
PING google.com (216.58.216.142) 56(84) bytes of data.
64 bytes from sea15s01-in-f14.1e100.net (216.58.216.142): icmp_req=1 ttl=61 time=7.18 ms

--- google.com ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 7.180/7.180/7.180/0.000 ms
```

Sucess!


### Part 2: Logging & Processing Firewall Events

*Given the setting for part I, you should set the firewall rules (on your gateway) to log certain events associated with incoming flows and then write a simple parser to report on basic statistics of the observed events. For example, you could log everything that is not whitelisted and generate a report on that log. Your report shows the total number of requests their break down across different types of protocols (http, https, ...), the frequency pif request fro each external IP address, etc. Note that you need to produce this incoming flows from another external nodes towards your gateway (or it might be generated by other external entities that are always scanning the network :-)*

Install the ```iptables-persistent``` package to make the iptables persistent across boots. The rules will be saved and loaded from __/etc/iptables/rules.v4__.

Add the logging for the all the traffic from the other machines from eth1 interface:

```
root@precise32:/home/vagrant# iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:ssh

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination
ACCEPT     all  --  anywhere             anywhere             state RELATED,ESTABLISHED
LOGGING    all  --  anywhere             anywhere

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination

Chain LOGGING (1 references)
target     prot opt source               destination
LOG        all  --  anywhere             anywhere             limit: avg 2/min burst 5 LOG level warning prefix "IPTables-NAT: "
ACCEPT     all  --  anywhere             anywhere
```

The saved iptable rules are the following:

```
# Generated by iptables-save v1.4.12 on Tue May 12 05:27:09 2015
*nat
:PREROUTING ACCEPT [36:6968]
:INPUT ACCEPT [31:6652]
:OUTPUT ACCEPT [61:4366]
:POSTROUTING ACCEPT [0:0]
-A POSTROUTING -o eth0 -j MASQUERADE
COMMIT
# Completed on Tue May 12 05:27:09 2015
# Generated by iptables-save v1.4.12 on Tue May 12 05:27:09 2015
*filter
:INPUT ACCEPT [30:3509]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [112:11803]
:LOGGING - [0:0]
-A INPUT -i eth0 -p tcp -m tcp --dport 22 -j ACCEPT
-A FORWARD -i eth0 -o eth1 -m state --state RELATED,ESTABLISHED -j ACCEPT
-A FORWARD -i eth1 -o eth0 -j LOGGING
-A LOGGING -m limit --limit 2/min -j LOG --log-prefix "IPTables-NAT: "
-A LOGGING -j ACCEPT
COMMIT
# Completed on Tue May 12 05:27:09 2015
```

Once we do a ```apt-get update``` on our host machine, we will see the following lines appear on our gateway machine's syslog:

```
May 12 06:02:02 precise32 kernel: [ 2966.745947] IPTables-NAT: IN=eth1 OUT=eth0 MAC=08:00:27:24:9c:d6:08:00:27:0a:8c:1c:08:00 SRC=192.168.0.203 DST=91.189.91.24 LEN=60 TOS=0x00 PREC=0x00 TTL=63 ID=14914 DF PROTO=TCP SPT=59470 DPT=80 WINDOW=14600 RES=0x00 SYN URGP=0
May 12 06:02:02 precise32 kernel: [ 2966.762489] IPTables-NAT: IN=eth1 OUT=eth0 MAC=08:00:27:24:9c:d6:08:00:27:0a:8c:1c:08:00 SRC=192.168.0.203 DST=91.189.91.13 LEN=60 TOS=0x00 PREC=0x00 TTL=63 ID=53915 DF PROTO=TCP SPT=36954 DPT=80 WINDOW=14600 RES=0x00 SYN URGP=0
May 12 06:02:02 precise32 kernel: [ 2966.824255] IPTables-NAT: IN=eth1 OUT=eth0 MAC=08:00:27:24:9c:d6:08:00:27:0a:8c:1c:08:00 SRC=192.168.0.203 DST=91.189.91.24 LEN=40 TOS=0x00 PREC=0x00 TTL=63 ID=14915 DF PROTO=TCP SPT=59470 DPT=80 WINDOW=14600 RES=0x00 ACK URGP=0
May 12 06:02:02 precise32 kernel: [ 2966.824422] IPTables-NAT: IN=eth1 OUT=eth0 MAC=08:00:27:24:9c:d6:08:00:27:0a:8c:1c:08:00 SRC=192.168.0.203 DST=91.189.91.24 LEN=246 TOS=0x00 PREC=0x00 TTL=63 ID=14916 DF PROTO=TCP SPT=59470 DPT=80 WINDOW=14600 RES=0x00 ACK PSH URGP=0
May 12 06:02:02 precise32 kernel: [ 2966.841381] IPTables-NAT: IN=eth1 OUT=eth0 MAC=08:00:27:24:9c:d6:08:00:27:0a:8c:1c:08:00 SRC=192.168.0.203 DST=91.189.91.13 LEN=40 TOS=0x00 PREC=0x00 TTL=63 ID=53916 DF PROTO=TCP SPT=36954 DPT=80 WINDOW=14600 RES=0x00 ACK URGP=0
```

The packets contains the following common information:

* source IP: 192.168.0.203 (our host's IP)
* destination IP: 91.189.91.24 (Ubuntu repository server's IP)
* destination port: 80 (they were HTTP requests)


We can grep all the NAT requests by filtering the log with keyword "IPTables-NAT", which was specified by our iptables rules. We can then aggregated all the information based on the following fields from the log entries:

* source IP
* dsetination IP
* protocol (TCP or UDP)
* destination port
* TCP SYN or ACK

It is hard to generate a lot of data on a virtual machine. 
Therefore, I decided to implement such a logging system on my own lab machine, which should have decent amount of web traffic throughout the day. 
Then I can collect some more information about the usage statistics by parsing the log data.
