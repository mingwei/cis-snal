# CIS 610 SNAL Project Report

## Mingwei Zhang, Lumin Shi

## Preparing MIST Cluster Environment

### Prepare Bootable USB Drives

1. Prepare bootable USB drives.
2. Install OSes on the two head nodes.
3. Install OSes on the other cluster nodes.

We chose to use Ubuntu Server 14.04 64 bit ([download link](http://releases.ubuntu.com/14.04.2/ubuntu-14.04.2-server-amd64.iso)) as the operating system. 
We use Mac OS as our working environment. 
We use the [Unetbootin](http://sourceforge.net/projects/unetbootin/?source=typ_redirect) to burn the ISO image to USB disks (3 in total).

![](figs/unetbootin.png)

### Install OSes on the machines