# Week 2 Report

Mingwei Zhang

## 1. Read something

[Overview of the Linux filesystem](http://tldp.org/LDP/intro-linux/html/sect_03_01.html)

*** 

## 2. Understanding existing disk partitions.

**Tasks:**

View the partitions on the following systems:

* your virtual machine
* the Raspberry Pi
* ix-trusty
* Optionally, if you are using a Mac, check its partitions, too

For each system:

 * how many disks do you see? 
 * how can you tell which ones are local and which ones are mounted over the network? 
 * briefly describe similarities and differences between system configurations in your report.


I can use the command ```mount```, ```df``` and ```fdisk -l``` command to lookup the mounted devices and file systems.

### My virtual machine

Here I did a ```fdisk -l``` on the virtual machine.
In fact, using ```fdisk``` is the best way to determine the disks. 
However, if you do not have the root permission, you cannot use this.

```
mingwei@hackvm:~$ sudo fdisk -l

Disk /dev/sda: 32.1 GB, 32142966784 bytes
255 heads, 63 sectors/track, 3907 cylinders, total 62779232 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x000b70b7

   Device Boot      Start         End      Blocks   Id  System
/dev/sda1   *        2048    60682239    30340096   83  Linux
/dev/sda2        60684286    62777343     1046529    5  Extended
/dev/sda5        60684288    62777343     1046528   82  Linux swap / Solaris
```

From the output, we can see that there is only one physical storage device,
and there are three partitions on this disk.

### The Raspberry Pi

Below is a output dump for a df command:

```
pi@duckpi02 ~ $ df -h
Filesystem      Size  Used Avail Use% Mounted on
rootfs           28G  2.5G   24G  10% /
/dev/root        28G  2.5G   24G  10% /
devtmpfs        460M     0  460M   0% /dev
tmpfs            93M  284K   93M   1% /run
tmpfs           5.0M     0  5.0M   0% /run/lock
tmpfs           186M     0  186M   0% /run/shm
/dev/mmcblk0p6   60M   15M   45M  25% /boot
/dev/sda1       7.3G  4.0K  7.3G   1% /media/KINGSTON
/dev/mmcblk0p5  488M  408K  452M   1% /media/data
/dev/mmcblk0p3   27M  397K   25M   2% /media/SETTINGS
/dev/sdb1       7.3G  4.0K  7.3G   1% /media/KINGSTON_
```

There are one SD disk and two USB drives plugged in the Pi.
Using the command ```ls /dev/sd*``` we can see how many external drives plugged in:

```
pi@duckpi02 ~ $ ls /dev/sd*
/dev/sda  /dev/sda1  /dev/sdb  /dev/sdb1
```


### ix-trusty

Since I do not have the root permission on ix, I just did a ```df``` on ix-trusty, and here is the part of the output.

```
mingwei@ix-trusty:~$ df -h
Filesystem                                        Size  Used Avail Use% Mounted on
/dev/vda1                                          24G   14G  8.6G  62% /
none                                              4.0K     0  4.0K   0% /sys/fs/cgroup
udev                                               16G  4.0K   16G   1% /dev
tmpfs                                             3.2G  596K  3.2G   1% /run
none                                              5.0M     0  5.0M   0% /run/lock
none                                               16G     0   16G   0% /run/shm
none                                              100M  4.0K  100M   1% /run/user
cranium.cs.uoregon.edu:/p/web/weblogs              24T   13G   24T   1% /cs/weblogs
cranium.cs.uoregon.edu:/p/local/linux              24T  2.0G   24T   1% /local
cranium.cs.uoregon.edu:/home/users/yuegu          2.0G   42M  2.0G   3% /home/users/yuegu
cranium.cs.uoregon.edu:/home/users/byeganeh       2.0G  1.6G  446M  79% /home/users/byeganeh
...
```

Looks like there are only one disk on ix-trusty, which has 24GB capacity.
All the other disks are mounted through network (potentially NFS),
and the location of the remote file server is ```cranium.cs.uoregon.edu```.

### Similarities and differences

All three configurations have multiple partitions on single disks.

Only ```ix-trusty``` mounts the remote folder onto it, and we can tell it by the location of the partition, 
which has a URL prefix ```cranium.cs.uoregon.edu:```

***

## 3. Expanding disk capacity.

**Tasks:**

* Refer to the [Linux HOWTO document](http://www.linuxhomenetworking.com/wiki/index.php/Quick_HOWTO_:_Ch27_:_Expanding_Disk_Capacity#.VR2yL2TF9FI) for this exercise.
* Connect the provided USB drive to your Pi or virtual machine and configure two new partitions on it, called /special and /data.
* Mount these partitions (interactively on the command line)
* Unmount and eject the USB drive
* Reinsert it and mount the partitions again
* Modify /etc/fstab and reboot your machine without removing the USB drive; verify that the new USB partitions are mounted correctly.
* Make a subdirectory in the /data partition and place some files there (text or pictures, up to you), and verify that unmount/eject/remount works and the files are still there.

### a. mount the USB drive

There are two USB drives inserted in our Raspberry Pi machine ```duckpi02```, and I use the ```/dev/sda``` device as my workspace.

As we plug in the USB drives, they are automatically mounted as shown below:

```
/dev/sdb1 on /media/KINGSTON_ type vfat (rw,nosuid,nodev,relatime,uid=1000,gid=1000,fmask=0022,dmask=0077,codepage=437,iocharset=ascii,shortname=mixed,showexec,utf8,flush,errors=remount-ro,uhelper=udisks)
/dev/sda1 on /media/MingweiDisk type vfat (rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=ascii,shortname=mixed,errors=remount-ro)
```

Note that the ```/dev/sda1``` is my disk.

### b. partition the USB drive and mount them

In order to partition the USB drive, we will first ```umount``` the mounted partition by:

```
sudo umount /dev/sda1
```

Then we will use ```fdisk``` to partion the USB drive into two partitions, and mount them on ```/special``` and ```/data```.

Currently, there is only one partition on this USB drive:

```
   Device Boot      Start         End      Blocks   Id  System
/dev/sda1              63    15148223     7574080+   b  W95 FAT32
```

We first delete the partition using ```d``` command in ```fdisk```. Then we create a new partition with 5GB capacity on it.

```
Command (m for help): n
Partition type:
   p   primary (0 primary, 0 extended, 4 free)
   e   extended
Select (default p): p
Partition number (1-4, default 1):
Using default value 1
First sector (2048-15148607, default 2048):
Using default value 2048
Last sector, +sectors or +size{K,M,G} (2048-15148607, default 15148607): +5G
```

Then we allocate the rest of the disk capacity to another partition.

Now we can have a look at the partition table of the USB drive:

```
   Device Boot      Start         End      Blocks   Id  System
/dev/sda1            2048    10487807     5242880   83  Linux
/dev/sda2        10487808    15148607     2330400   83  Linux
```

After we are sure that the partition looks good, we can then write out the partition table to the disk using command ```w```. 

```
Command (m for help): w
The partition table has been altered!

Calling ioctl() to re-read partition table.
Syncing disks.
```

Before we can actually mount them on the pi, we need to create file systems on the two partitions. Here we choose to use the default Linux file system ```ext3``` as the format for the two partitions. Here we only show the output for disk ```/dev/sda1```.

```
pi@duckpi02 ~ $ sudo mkfs -t ext3 /dev/sda1
mke2fs 1.42.5 (29-Jul-2012)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
327680 inodes, 1310720 blocks
65536 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=1342177280
40 block groups
32768 blocks per group, 32768 fragments per group
8192 inodes per group
Superblock backups stored on blocks:
	32768, 98304, 163840, 229376, 294912, 819200, 884736

Allocating group tables: done
Writing inode tables: done
Creating journal (32768 blocks): done
Writing superblocks and filesystem accounting information: done
```

Now we can mount the two partitions ```/dev/sda1``` and ```/dev/sda2``` to the folders ```/special``` and ```/data``` (of course we need to create these two folders first) using ```mount``` command. Then we show the mounted partition using ```df``` command (unrelated mounting points are ignored).

```
pi@duckpi02 ~ $ sudo mount /dev/sda1 /special
pi@duckpi02 ~ $ sudo mount /dev/sda2 /data
pi@duckpi02 ~ $ df
Filesystem     1K-blocks    Used Available Use% Mounted on
...
/dev/sda1        5029504   10364   4756996   1% /special
/dev/sda2        2228240    3500   2108220   1% /data
...
```

### c. unmount, eject, reinsert, mount the USB disk

In order to verify the partitions were created and mounted correctly, we will create one file on each partition and unmount, eject, reinsert, mount the USB disk.

We first give our user permission to change data on the partitions we created.

```
pi@duckpi02 ~ $ sudo chown pi /special
pi@duckpi02 ~ $ sudo chown pi /data
```

Then we add two files on the partitions, and we will verify the file content after we've reinserted the USB drive.

```
pi@duckpi02 ~ $ echo "file on /special by Mingwei" > /special/mingwei.txt
pi@duckpi02 ~ $ echo "file on /data by Mingwei" > /data/mingwei.txt
pi@duckpi02 ~ $ cat /special/mingwei.txt
file on /special by Mingwei
pi@duckpi02 ~ $ cat /data/mingwei.txt
file on /data by Mingwei
```

Now we can unmount the two partitions and unplug and reinsert the USB drive.

```
pi@duckpi02 ~ $ sudo umount /dev/sda1
pi@duckpi02 ~ $ sudo umount /dev/sda2
```

By the default setting of this pi, the partitions on the USB drives will be automatically mounted:

```
pi@duckpi02 ~ $ df
Filesystem     1K-blocks    Used Available Use% Mounted on
...
/dev/sda1        5029504   10368   4756992   1% /media/e1a486a4-3232-43be-8f51-319a4ab9836f
/dev/sda2        2228240    3504   2108216   1% /media/dd02036d-d02b-4a9b-b343-26c5883baa5b
...
```

But we can manually remount them on to the places we want them to be, and verify the files:

```
pi@duckpi02 ~ $ sudo umount /dev/sda1
pi@duckpi02 ~ $ sudo umount /dev/sda2

pi@duckpi02 ~ $ sudo mount /dev/sda1 /special
pi@duckpi02 ~ $ sudo mount /dev/sda2 /data

pi@duckpi02 ~ $ cat /special/mingwei.txt
file on /special by Mingwei
pi@duckpi02 ~ $ cat /data/mingwei.txt
file on /data by Mingwei

pi@duckpi02 ~ $ df
Filesystem     1K-blocks    Used Available Use% Mounted on
...
/dev/sda1        5029504   10368   4756992   1% /special
/dev/sda2        2228240    3504   2108216   1% /data
...
```

### d. modify /etc/fstab to save the partition mounting options

In ordre to save the partition table and automatically mount them after reboot, we need to change the ```/etc/fstab``` file. I've added the two lines to automatically mount the partitions to ```/special``` and ```/data```:

```
proc            /proc           proc    defaults          0       0
/dev/mmcblk0p6  /boot           vfat    defaults          0       2
/dev/mmcblk0p7  /               ext4    defaults,noatime  0       1
# a swapfile is not a swap partition, so no using swapon|off from here on, use  dphys-swapfile swap[on|off]  for that
/dev/sda1  /special  ext3  defaults 1 2
/dev/sda2  /data  ext3  defaults 1 2
```

The I restarted the pi to verify the correctness of the configuration:

```
pi@duckpi02 ~ $ sudo reboot

Broadcast message from root@duckpi02 (pts/0) (Wed Apr  8 20:58:44 2015):

The system is going down for reboot NOW!
Connection to duckpi02.d.cs.uoregon.edu closed by remote host.
Connection to duckpi02.d.cs.uoregon.edu closed.
```

After the reboot, we can take a look at the mounted file systems and verify the files we created before the reboot:

```
pi@duckpi02 ~ $ df
Filesystem     1K-blocks    Used Available Use% Mounted on
...
/dev/sda1        5029504   10368   4756992   1% /special
/dev/sda2        2228240    3504   2108216   1% /data
...

pi@duckpi02 ~ $ cat /special/mingwei.txt
file on /special by Mingwei
pi@duckpi02 ~ $ cat /data/mingwei.txt
file on /data by Mingwei
```

Looks like all the file systems were automatically mounted during boot, and the files are intact.

* * *

## 4. Disk space problems.

**Tasks:**

* On your Pi, mostly fill up /dev/root partition (e.g., by creating multiple copies of a very large file; dd and yes are two other commands that can "help" with that). What happens when you approach 100% use? For example, try creating a new file, looking up help info for some commands, or doing any other command-line activities.
* Make various subdirectories and spread files of different sizes among them. Then, preferably on another team's Pi, find out the most recently created, most space-consuming directories (and files). If you are doing this outside class, then work in pairs and have one person "hide" the large files and the other team member "find" them, then swap and repeat.
* Suppose those big files you added are really necessary and you cannot just delete them. How would you fix the problem?
* Write a program that opens and writes to a file (but does not close it and keeps running). Before you run the program, check the current directory size (e.g., du -sk .) and record that number. Then, delete the file manually from the file system while your program is still writing to it. You can also do this by manually editing a file, but make sure to add substantial amounts of text. Discuss what happens. Can you see the file with ls? What about lsof? Even more advanced: use syslog to create a system log file and keep writing to it after deleting from file system.


### a. fill up /dev/root partition

According to the output of ```df```, the /home/pi folder should be within the ```/dev/root``` partition. In order to fill the partition up, we can just create a big file using ```yes``` and ```dd```commands.

At my home directory, I can do the following command to achieve:

```
yes "this is a big file" | dd of /home/pi/bigfile_mingwei bs=4k conv=notrunc
```

We can fill the entire disk up and then take a look at the partition usage using ```df```:

```
pi@duckpi02 ~ $ df -h
Filesystem      Size  Used Avail Use% Mounted on
rootfs           28G   27G     0 100% /
/dev/root        28G   27G     0 100% /
...
```

Yes, we got to 100%. Now what happens? Nothing will happen until you're trying to write something to the disk. Here is a example of open a file in ```vim```. Since it requires creating a temporary file for when editing a file, the creation part will fail (though you can still go ahead and edit the file anyway).

![](https://dl.dropboxusercontent.com/u/13917744/Screenshot%202015-04-09%2019.05.26.png)

Now if you try to write something to the file you just opened, even if you removed lines of the file, it will fail:

![](https://dl.dropboxusercontent.com/u/13917744/Screenshot%202015-04-09%2019.10.43.png)

### b. find most recently created, most space-consuming directories.

Most recently created directories (top 10), using ```find``` command:

```
pi@duckpi02 ~ $ find . -type d -printf "%T+ %p\n" | sort | head -10
1970-01-01+00:12:33.2999997120 ./python_games
2015-02-16+14:10:16.8099999950 ./.dbus
2015-02-16+14:10:16.8099999950 ./.dbus/session-bus
2015-02-16+14:10:16.8399999950 ./.config/pcmanfm
2015-02-16+14:10:16.8699999950 ./.config/openbox
2015-02-16+14:10:16.8899999950 ./.config/lxpanel
2015-02-16+14:10:16.8899999950 ./.config/lxpanel/LXDE-pi
2015-02-16+14:10:16.8999999950 ./.config/lxpanel/LXDE-pi/panels
2015-02-16+14:10:16.8999999950 ./Desktop
2015-02-16+14:10:17.3699999950 ./.cache/openbox/sessions
```
In this command, we first specify that we want to look for directories. After found all the directories, we will print the creation time and the directory name. Then we pipe the output to ```sort``` command to sort by date. Last, we only shows top 10 findings by pipe the sorted results into ```head```.

Most space-consuming directories (top 10), using ```du``` command:

```
pi@duckpi02 ~ $ sudo du --max-depth 0 /*/ | sort -nr | head -10
24938040        /home/
1584372 /usr/
567584  /opt/
331472  /var/
110736  /lib/
14541   /boot/
6508    /sbin/
5304    /bin/
4748    /etc/
292     /run/
```

In this line, we use ```du``` to access the basic directory size information. Since ```du``` will automatically output the size, we can just sort the output by numerical order, and then take the top 10 results.

### c. how to securely deal with important big files on small partition?

We can insert a bigger data drive and move the file to the data drive.
After moving it, we can then create a soft link and point it back to where it was. As a result, the programs that use the big files can still access it using the same path, and we store the actual files on another drive.

```
ln -s TARGET LINK_NAME
```


### d. open a file, remove it, and write to it. what will happen?

Before any action, I ran the command ```du``` to get the current folder size:

```
pi@duckpi02 /data_mingwei $ ls
write_file.py
pi@duckpi02 /data_mingwei $ du -sk .
8       .
```

Then I wrote a simple python script to periodically write 10000 "x" to a file called "writeto.txt". Here is the python script:

```python
#!/usr/bin/python

import time

f = open("writeto.txt","w")

while True:
        f.write("x"*10000)
        time.sleep(5)

f.close()
```

The size of the current folder will keep increasing every 5 seconds:

```
pi@duckpi02 /data_mingwei $ du -sk .
16      .
pi@duckpi02 /data_mingwei $ du -sk .
24      .
pi@duckpi02 /data_mingwei $ du -sk .
36      .
pi@duckpi02 /data_mingwei $ du -sk .
44      .
```

However, after I removed the file that the program is currently write to, the current folder's size will become again back to the orginal size, regarless the program that still keeps writing to it.

```
pi@duckpi02 /data_mingwei $ rm writeto.txt
pi@duckpi02 /data_mingwei $ du -sk .
8       .
```

I don't know where the content has been written to, so I ran the ```lsof``` command to see if some program was still using the ```writeto.txt``` file:

```
pi@duckpi02 /data_mingwei $ lsof |grep writeto
python    11133              pi    3w      REG        8,2   479232      11 /data_mingwei/writeto.txt (deleted)
```

Apparently, the OS has detected that the file that the python program was writing to had already been deleted. So I suppose there are no actual file oepration going on now.

## 5. Filesystem objects

**Tasks:**

Find a few examples of filesystem objects that are not data files on your system(s) and describe what they are briefly in your report.

### find objects that are not data files

We can use the command ```find``` to find various types of files. Here is a list of types shown in ```find``` command's man page:

```
       -type c
              File is of type c:
              b      block (buffered) special
              c      character (unbuffered) special
              d      directory
              p      named pipe (FIFO)
              f      regular file
              l      symbolic  link;  this is never true if the -L option or the -follow
                     option is in effect, unless the symbolic link is  broken.   If  you
                     want to search for symbolic links when -L is in effect, use -xtype.
              s      socket
              D      door (Solaris)
```

#### sockets

```
pi@duckpi02 /data_mingwei $ sudo find / -type s
/dev/log
/tmp/.menu-cached-:0-pi
/tmp/.X11-unix/X0
/tmp/ssh-2u7IfxyHuR8T/agent.2387
/tmp/.pcmanfm-socket--0-pi
/run/dbus/system_bus_socket
...
```

The socket type files are network related sockets, used for communicating with other machines and programs.

#### blocks

```
pi@duckpi02 /data_mingwei $ sudo find / -type b
/dev/sdb2
/dev/sdb1
/dev/sdb
/dev/sda2
/dev/sda1
/dev/sda
/dev/mmcblk0p7 
/dev/mmcblk0p6
/dev/mmcblk0p5
/dev/mmcblk0p3
/dev/mmcblk0p2
/dev/mmcblk0p1
/dev/mmcblk0
...
```

As we can see, the block type files are likely to be the representation of the storage drive devices, such as hard drives, USB drives, SD cards, etc.

#### characters

```
pi@duckpi02 /data_mingwei $ sudo find / -type c
...
/dev/tty63
/dev/tty62
/dev/tty61
/dev/tty60
/dev/tty59
/dev/tty58
/dev/tty57
/dev/tty56
/dev/tty55
/dev/tty54
...
```

The character type files are more likely to be those who deals with character-wise input outputs, such as the ```/dev/ttyXX```, which are basically all the commandline terminal interfaces.

#### pipes

```
pi@duckpi02 /data_mingwei $ sudo find / -type p
/dev/xconsole
/run/initctl
```

I have no idea what the pipes are for.

## 6. RAID disks.

**Tasks:**

* For this exercise, it is best if you use two USB sticks -- team up with someone to work on each Pi or use an extra USB drive of your own.
* Reading: [RAID HOWTO](http://www.linuxhomenetworking.com/wiki/index.php/Quick_HOWTO_:_Ch26_:_Linux_Software_RAID#.VR2_2GTF9FI)
* You may need to install the mdadm package; do not change default options during installation.
* Create a RAID 0 set; include the results of cat /proc/mdstat, mdadm --detail --scan and the contents of /etc/fstab in your report before you do the next step.
* Remove the RAID 0 set and create a RAID 1 set, include the same information about that as you did for RAID 0 in your report.

Before:

```
pi@duckpi02 /data_mingwei $ sudo mdadm --detail --scan
pi@duckpi02 /data_mingwei $ sudo cat /proc/mdstat,
cat: /proc/mdstat,: No such file or directory
```

### RAID 0:

Create new partitions on both USB drives we've plugged in. When selecting the type of the drive, we choose ```fd  Linux raid auto``` to enable RAID.

```
Command (m for help): n
Partition type:
   p   primary (0 primary, 0 extended, 4 free)
   e   extended
Select (default p):
Using default response p
Partition number (1-4, default 1):
Using default value 1
First sector (2048-15148607, default 2048):
Using default value 2048
Last sector, +sectors or +size{K,M,G} (2048-15148607, default 15148607):
Using default value 15148607

Command (m for help): t
Selected partition 1
Hex code (type L to list codes): L

 0  Empty           24  NEC DOS         81  Minix / old Lin bf  Solaris
 1  FAT12           27  Hidden NTFS Win 82  Linux swap / So c1  DRDOS/sec (FAT-
 2  XENIX root      39  Plan 9          83  Linux           c4  DRDOS/sec (FAT-
 3  XENIX usr       3c  PartitionMagic  84  OS/2 hidden C:  c6  DRDOS/sec (FAT-
 4  FAT16 <32M      40  Venix 80286     85  Linux extended  c7  Syrinx
 5  Extended        41  PPC PReP Boot   86  NTFS volume set da  Non-FS data
 6  FAT16           42  SFS             87  NTFS volume set db  CP/M / CTOS / .
 7  HPFS/NTFS/exFAT 4d  QNX4.x          88  Linux plaintext de  Dell Utility
 8  AIX             4e  QNX4.x 2nd part 8e  Linux LVM       df  BootIt
 9  AIX bootable    4f  QNX4.x 3rd part 93  Amoeba          e1  DOS access
 a  OS/2 Boot Manag 50  OnTrack DM      94  Amoeba BBT      e3  DOS R/O
 b  W95 FAT32       51  OnTrack DM6 Aux 9f  BSD/OS          e4  SpeedStor
 c  W95 FAT32 (LBA) 52  CP/M            a0  IBM Thinkpad hi eb  BeOS fs
 e  W95 FAT16 (LBA) 53  OnTrack DM6 Aux a5  FreeBSD         ee  GPT
 f  W95 Ext'd (LBA) 54  OnTrackDM6      a6  OpenBSD         ef  EFI (FAT-12/16/
10  OPUS            55  EZ-Drive        a7  NeXTSTEP        f0  Linux/PA-RISC b
11  Hidden FAT12    56  Golden Bow      a8  Darwin UFS      f1  SpeedStor
12  Compaq diagnost 5c  Priam Edisk     a9  NetBSD          f4  SpeedStor
14  Hidden FAT16 <3 61  SpeedStor       ab  Darwin boot     f2  DOS secondary
16  Hidden FAT16    63  GNU HURD or Sys af  HFS / HFS+      fb  VMware VMFS
17  Hidden HPFS/NTF 64  Novell Netware  b7  BSDI fs         fc  VMware VMKCORE
18  AST SmartSleep  65  Novell Netware  b8  BSDI swap       fd  Linux raid auto
1b  Hidden W95 FAT3 70  DiskSecure Mult bb  Boot Wizard hid fe  LANstep
1c  Hidden W95 FAT3 75  PC/IX           be  Solaris boot    ff  BBT
1e  Hidden W95 FAT1 80  Old Minix
Hex code (type L to list codes): fd
Changed system type of partition 1 to fd (Linux raid autodetect)


Command (m for help): p

Disk /dev/sda: 7756 MB, 7756087296 bytes
239 heads, 62 sectors/track, 1022 cylinders, total 15148608 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x6e427468

   Device Boot      Start         End      Blocks   Id  System
/dev/sda1            2048    15148607     7573280   fd  Linux raid autodetect
```

Then we use ```mdadm``` to create a RAID0 using ```/dev/sda1``` and ```/dev/sdb1```.

```
root@duckpi02:/home/pi# mdadm --create --verbose /dev/md/snalraid0 --level=0 --raid-devices=2 /dev/sda1 /dev/sdb1
mdadm: chunk size defaults to 512K
mdadm: /dev/sda1 appears to contain an ext2fs file system
    size=5242880K  mtime=Wed Apr  8 23:02:39 2015
mdadm: /dev/sdb1 appears to contain an ext2fs file system
    size=3573280K  mtime=Thu Apr  9 17:53:40 2015
Continue creating array? y
mdadm: Defaulting to version 1.2 metadata
mdadm: array /dev/md/snalraid0 started.
```

We then test the RAID0's running status:

```
root@duckpi02:/home/pi# cat /proc/mdstat
Personalities : [raid0]
md127 : active raid0 sdb1[1] sda1[0]
      15145984 blocks super 1.2 512k chunks

unused devices: <none>
```

```
root@duckpi02:/home/pi# mdadm --detail --scan
ARRAY /dev/md/snalraid0 metadata=1.2 name=duckpi02:snalraid0 UUID=5b49f176:6ee53d57:c4becbec:cf0b7f3b
```

Note that, in order to mount the RAID device, we also need to create file system on the device. Here we use ```mkfs.ext4``` command to create a ```ext4``` file system on the RAID0 device:

```
root@duckpi02:/home/pi# mkfs.ext4 /dev/md/snalraid0
mke2fs 1.42.5 (29-Jul-2012)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=128 blocks, Stripe width=256 blocks
948416 inodes, 3786496 blocks
189324 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=3879731200
116 block groups
32768 blocks per group, 32768 fragments per group
8176 inodes per group
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208
 
Allocating group tables: done
Writing inode tables: done
Creating journal (32768 blocks): done
Writing superblocks and filesystem accounting information:
```

Then we can mount the device and show it using ```df``` command.

```
root@duckpi02:/home/pi# df
Filesystem     1K-blocks     Used Available Use% Mounted on
rootfs          29094896 17980432   9613488  66% /
/dev/root       29094896 17980432   9613488  66% /
devtmpfs          470368        0    470368   0% /dev
tmpfs              94936      300     94636   1% /run
tmpfs               5120        0      5120   0% /run/lock
tmpfs             189860        0    189860   0% /run/shm
/dev/mmcblk0p6     60479    14541     45939  25% /boot
/dev/mmcblk0p5    499656      408    462552   1% /media/data
/dev/mmcblk0p3     27633      397     24943   2% /media/SETTINGS
/dev/md127      14776800    36984  13966136   1% /raid0
```
Here is a full copy of our ```/etc/fstab``` file:

```
root@duckpi02:/home/pi# cat /etc/fstab
proc            /proc           proc    defaults          0       0
/dev/mmcblk0p6  /boot           vfat    defaults          0       2
/dev/mmcblk0p7  /               ext4    defaults,noatime  0       1
# a swapfile is not a swap partition, so no using swapon|off from here on, use  dphys-swapfile swap[on|off]  for that

/dev/md/snalraid0      /raid0    ext4    defaults    1 2

# mingwei's usb drive
#/dev/sda1  /special_mingwei  ext3  defaults 1 2
#/dev/sda2  /data_mingwei  ext3  defaults 1 2

# lumin's usb drive
#/dev/sdb1  /special ext3  defaults 1 2
#/dev/sdb2  /data ext3  defaults 1 2
```

### RAID1

Before we proceed on creating a RAID1, we need to stop the current running RAID0 first, using ```mdadm```:

```
root@duckpi02:/home/pi# mdadm --stop /dev/md/snalraid0
mdadm: stopped /dev/md/snalraid0
root@duckpi02:/home/pi# cat /proc/mdstat
Personalities : [raid0]
unused devices: <none>
```

Then using the similar command, we create the RAID1 partition:

```
root@duckpi02:/home/pi# mdadm --create --verbose /dev/md/snalraid1 --level=1 --raid-devices=2 /dev/sda1 /dev/sdb1
mdadm: /dev/sda1 appears to contain an ext2fs file system
    size=5242880K  mtime=Wed Apr  8 23:02:39 2015
mdadm: /dev/sda1 appears to be part of a raid array:
    level=raid0 devices=2 ctime=Fri Apr 10 03:07:05 2015
mdadm: Note: this array has metadata at the start and
    may not be suitable as a boot device.  If you plan to
    store '/boot' on this device please ensure that
    your boot-loader understands md/v1.x metadata, or use
    --metadata=0.90
mdadm: /dev/sdb1 appears to contain an ext2fs file system
    size=3573280K  mtime=Thu Apr  9 17:53:40 2015
mdadm: /dev/sdb1 appears to be part of a raid array:
    level=raid0 devices=2 ctime=Fri Apr 10 03:07:05 2015
mdadm: size set to 7569152K
Continue creating array? y
mdadm: Defaulting to version 1.2 metadata
mdadm: array /dev/md/snalraid1 started.
root@duckpi02:/home/pi#
```

Here we show the running status of the RAID1:

```
root@duckpi02:/home/pi# !cat
cat /proc/mdstat
Personalities : [raid0] [raid1]
md127 : active raid1 sdb1[1] sda1[0]
      7569152 blocks super 1.2 [2/2] [UU]
      [>....................]  resync =  1.0% (81216/7569152) finish=15.3min speed=8121K/sec

unused devices: <none>
```

```
root@duckpi02:/home/pi# mdadm --detail --scan
ARRAY /dev/md/snalraid1 metadata=1.2 name=duckpi02:snalraid1 UUID=e7aac48a:7611ae97:7509f9d3:000d4457
```
The ```/etc/fstab``` is quite similar, just change the mounting source and mounting location of the RAID device:

```
/dev/md/snalraid1      /raid1    ext4    defaults    1 2

```
